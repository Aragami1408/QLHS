﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDMCanBoLop : Form
    {
        public fDMCanBoLop()
        {
            InitializeComponent();
        }
        
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void fDMCanBoLop_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi);
            try
            {
                dataGridView1.Rows.Clear();
                con.Open();
                string select = "select * from SYLL where NhiemVu <> '' ";
                SqlCommand cmd = new SqlCommand(select, con);
                SqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    dataGridView1.Rows.Add(rd["MaHS"].ToString(), rd["TenHS"].ToString(), rd["NhiemVu"].ToString(),rd["DienThoai"].ToString(), rd["NoiO"].ToString());
                }

                con.Close();
                con.Dispose();
            }
            catch (SqlException sqlexp)
            {
                MessageBox.Show(sqlexp.Message, "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
