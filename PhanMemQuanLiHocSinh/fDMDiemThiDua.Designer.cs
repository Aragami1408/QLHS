﻿namespace PhanMemQuanLiHocSinh
{
    partial class fDMDiemThiDua
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTieuDe = new System.Windows.Forms.Label();
            this.lbMaHocSinh = new System.Windows.Forms.Label();
            this.lbTenHocSInh = new System.Windows.Forms.Label();
            this.lbNoiDung = new System.Windows.Forms.Label();
            this.tbDiemCong = new System.Windows.Forms.TextBox();
            this.lbDiemCong = new System.Windows.Forms.Label();
            this.lbDiemTru = new System.Windows.Forms.Label();
            this.tbMaHS = new System.Windows.Forms.TextBox();
            this.tbTenHocSinh = new System.Windows.Forms.TextBox();
            this.tbDiemTru = new System.Windows.Forms.TextBox();
            this.tbNoiDungThiDua = new System.Windows.Forms.TextBox();
            this.btLuuVaTinhDiemThiDua = new System.Windows.Forms.Button();
            this.btClose = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbTieuDe
            // 
            this.lbTieuDe.AutoSize = true;
            this.lbTieuDe.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.tableLayoutPanel1.SetColumnSpan(this.lbTieuDe, 2);
            this.lbTieuDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTieuDe.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTieuDe.ForeColor = System.Drawing.Color.White;
            this.lbTieuDe.Location = new System.Drawing.Point(0, 0);
            this.lbTieuDe.Margin = new System.Windows.Forms.Padding(0);
            this.lbTieuDe.Name = "lbTieuDe";
            this.lbTieuDe.Size = new System.Drawing.Size(484, 50);
            this.lbTieuDe.TabIndex = 0;
            this.lbTieuDe.Text = "DANH MỤC ĐIỂM THI ĐUA";
            this.lbTieuDe.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbMaHocSinh
            // 
            this.lbMaHocSinh.AutoSize = true;
            this.lbMaHocSinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMaHocSinh.Location = new System.Drawing.Point(4, 50);
            this.lbMaHocSinh.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbMaHocSinh.Name = "lbMaHocSinh";
            this.lbMaHocSinh.Size = new System.Drawing.Size(154, 35);
            this.lbMaHocSinh.TabIndex = 1;
            this.lbMaHocSinh.Text = "Mã học sinh";
            this.lbMaHocSinh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTenHocSInh
            // 
            this.lbTenHocSInh.AutoSize = true;
            this.lbTenHocSInh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTenHocSInh.Location = new System.Drawing.Point(4, 85);
            this.lbTenHocSInh.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTenHocSInh.Name = "lbTenHocSInh";
            this.lbTenHocSInh.Size = new System.Drawing.Size(154, 35);
            this.lbTenHocSInh.TabIndex = 2;
            this.lbTenHocSInh.Text = "Tên học sinh";
            this.lbTenHocSInh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbNoiDung
            // 
            this.lbNoiDung.AutoSize = true;
            this.lbNoiDung.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbNoiDung.Location = new System.Drawing.Point(4, 120);
            this.lbNoiDung.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbNoiDung.Name = "lbNoiDung";
            this.lbNoiDung.Size = new System.Drawing.Size(154, 35);
            this.lbNoiDung.TabIndex = 3;
            this.lbNoiDung.Text = "Nội dung thi đua";
            this.lbNoiDung.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbDiemCong
            // 
            this.tbDiemCong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDiemCong.Location = new System.Drawing.Point(166, 159);
            this.tbDiemCong.Margin = new System.Windows.Forms.Padding(4);
            this.tbDiemCong.Name = "tbDiemCong";
            this.tbDiemCong.Size = new System.Drawing.Size(314, 27);
            this.tbDiemCong.TabIndex = 7;
            // 
            // lbDiemCong
            // 
            this.lbDiemCong.AutoSize = true;
            this.lbDiemCong.CausesValidation = false;
            this.lbDiemCong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDiemCong.Location = new System.Drawing.Point(4, 155);
            this.lbDiemCong.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbDiemCong.Name = "lbDiemCong";
            this.lbDiemCong.Size = new System.Drawing.Size(154, 35);
            this.lbDiemCong.TabIndex = 8;
            this.lbDiemCong.Text = "Điểm cộng";
            this.lbDiemCong.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDiemTru
            // 
            this.lbDiemTru.AutoSize = true;
            this.lbDiemTru.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDiemTru.Location = new System.Drawing.Point(4, 190);
            this.lbDiemTru.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbDiemTru.Name = "lbDiemTru";
            this.lbDiemTru.Size = new System.Drawing.Size(154, 35);
            this.lbDiemTru.TabIndex = 9;
            this.lbDiemTru.Text = "Điểm trừ";
            this.lbDiemTru.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbMaHS
            // 
            this.tbMaHS.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbMaHS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMaHS.Location = new System.Drawing.Point(166, 54);
            this.tbMaHS.Margin = new System.Windows.Forms.Padding(4);
            this.tbMaHS.Name = "tbMaHS";
            this.tbMaHS.ReadOnly = true;
            this.tbMaHS.Size = new System.Drawing.Size(314, 27);
            this.tbMaHS.TabIndex = 12;
            // 
            // tbTenHocSinh
            // 
            this.tbTenHocSinh.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tbTenHocSinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTenHocSinh.Location = new System.Drawing.Point(166, 89);
            this.tbTenHocSinh.Margin = new System.Windows.Forms.Padding(4);
            this.tbTenHocSinh.Name = "tbTenHocSinh";
            this.tbTenHocSinh.Size = new System.Drawing.Size(314, 27);
            this.tbTenHocSinh.TabIndex = 13;
            // 
            // tbDiemTru
            // 
            this.tbDiemTru.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDiemTru.Location = new System.Drawing.Point(166, 194);
            this.tbDiemTru.Margin = new System.Windows.Forms.Padding(4);
            this.tbDiemTru.Name = "tbDiemTru";
            this.tbDiemTru.Size = new System.Drawing.Size(314, 27);
            this.tbDiemTru.TabIndex = 15;
            // 
            // tbNoiDungThiDua
            // 
            this.tbNoiDungThiDua.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNoiDungThiDua.Location = new System.Drawing.Point(166, 124);
            this.tbNoiDungThiDua.Margin = new System.Windows.Forms.Padding(4);
            this.tbNoiDungThiDua.Name = "tbNoiDungThiDua";
            this.tbNoiDungThiDua.Size = new System.Drawing.Size(314, 27);
            this.tbNoiDungThiDua.TabIndex = 16;
            // 
            // btLuuVaTinhDiemThiDua
            // 
            this.btLuuVaTinhDiemThiDua.Location = new System.Drawing.Point(215, 4);
            this.btLuuVaTinhDiemThiDua.Margin = new System.Windows.Forms.Padding(4);
            this.btLuuVaTinhDiemThiDua.Name = "btLuuVaTinhDiemThiDua";
            this.btLuuVaTinhDiemThiDua.Size = new System.Drawing.Size(141, 28);
            this.btLuuVaTinhDiemThiDua.TabIndex = 18;
            this.btLuuVaTinhDiemThiDua.Text = "Tính điểm thi đua";
            this.btLuuVaTinhDiemThiDua.UseVisualStyleBackColor = true;
            this.btLuuVaTinhDiemThiDua.Click += new System.EventHandler(this.btLuuVaTinhDiemThiDua_Click);
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(364, 4);
            this.btClose.Margin = new System.Windows.Forms.Padding(4);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(116, 28);
            this.btClose.TabIndex = 19;
            this.btClose.Text = "Đóng";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.tbDiemTru, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbNoiDungThiDua, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbDiemCong, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lbTieuDe, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbMaHocSinh, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbTenHocSinh, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbTenHocSInh, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbMaHS, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbNoiDung, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbDiemCong, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lbDiemTru, 0, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(484, 312);
            this.tableLayoutPanel1.TabIndex = 21;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.btClose);
            this.flowLayoutPanel1.Controls.Add(this.btLuuVaTinhDiemThiDua);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 276);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(484, 36);
            this.flowLayoutPanel1.TabIndex = 22;
            // 
            // fDMDiemThiDua
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 312);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(500, 350);
            this.Name = "fDMDiemThiDua";
            this.Text = "Danh mục điểm thi đua";
            this.Load += new System.EventHandler(this.fDMDiemThiDua_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbTieuDe;
        private System.Windows.Forms.Label lbMaHocSinh;
        private System.Windows.Forms.Label lbTenHocSInh;
        private System.Windows.Forms.Label lbNoiDung;
        private System.Windows.Forms.TextBox tbDiemCong;
        private System.Windows.Forms.Label lbDiemCong;
        private System.Windows.Forms.Label lbDiemTru;
        private System.Windows.Forms.TextBox tbMaHS;
        private System.Windows.Forms.TextBox tbTenHocSinh;
        private System.Windows.Forms.TextBox tbDiemTru;
        private System.Windows.Forms.TextBox tbNoiDungThiDua;
        private System.Windows.Forms.Button btLuuVaTinhDiemThiDua;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}