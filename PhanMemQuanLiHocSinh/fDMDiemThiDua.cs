﻿using Phần_mềm_quản_lý_học_sinh;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDMDiemThiDua : Form
    {
        public fDMDiemThiDua()
        {
            InitializeComponent();
        }

        private void btSignOut_Click(object sender, EventArgs e)
        {
            Hide();
            fDangNhap f = new fDangNhap();
            f.Show();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
            Dispose();
        }



        private void fDMDiemThiDua_Load(object sender, EventArgs e)
        {
            string id = Guid.NewGuid().ToString();
            tbMaHS.Text = id;
        }

        private void btLuuVaTinhDiemThiDua_Click(object sender, EventArgs e)
        {

            SqlConnection conn = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi);
           
            if (String.IsNullOrWhiteSpace(tbMaHS.Text))
            {
                MessageBox.Show("Mời bạn nhập lại mã học sinh", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbTenHocSinh.Text))
            {
                MessageBox.Show("Mời bạn nhập lại tên học sinh", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbNoiDungThiDua.Text))
            {
                MessageBox.Show("Mời bạn nhập nội dung thi đua và đánh giá", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbDiemCong.Text))
            {
                MessageBox.Show("Mời bạn nhập điểm cộng", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbDiemTru.Text))
            {
                MessageBox.Show("Mời bạn nhập điểm trừ", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string XepLoai;
                int TongDiem = 100 + Convert.ToInt32(tbDiemCong.Text) - Convert.ToInt32(tbDiemTru.Text);
                if (TongDiem >= 110)
                {
                    XepLoai = "Tốt";
                }
                else if (TongDiem >= 86 && TongDiem <= 109)
                {
                    XepLoai = "Khá";
                }
                else if (TongDiem >=75 && TongDiem <= 86)
                {
                    XepLoai = "Trung Bình";
                }
                else
                {
                    XepLoai = "Yếu";
                }
                try
                {
                    conn.Open();
                    string query = "INSERT INTO DMThiDua (MaHS, TenHS, NoiDung, DiemCong, DiemTru, TongDiem) VALUES (@MaHS, @TenHS, @NoiDung, @DiemCong, @DiemTru, @TongDiem)";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@MaHS", tbMaHS.Text);
                    cmd.Parameters.AddWithValue("@TenHS", tbTenHocSinh.Text);
                    cmd.Parameters.AddWithValue("@NoiDung", tbNoiDungThiDua.Text);
                    cmd.Parameters.AddWithValue("@DiemCong", tbDiemCong.Text);
                    cmd.Parameters.AddWithValue("@DiemTru", tbDiemTru.Text);
                    cmd.Parameters.AddWithValue("@TongDiem", TongDiem);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    conn.Dispose();

                    MessageBox.Show("Điểm thi đua của bạn là " + TongDiem + ", Xếp Loại: " + XepLoai, "Phần mềm quản lý học sinh", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
                
                
            }

        }
    }
}
