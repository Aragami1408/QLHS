﻿namespace PhanMemQuanLiHocSinh
{
    partial class fDanhGiaHocSinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbMaHS = new System.Windows.Forms.Label();
            this.tbMaHS = new System.Windows.Forms.TextBox();
            this.tbNhanXetCMHS = new System.Windows.Forms.TextBox();
            this.tbNhanXetGVCN = new System.Windows.Forms.TextBox();
            this.tbNhanXetCBL = new System.Windows.Forms.TextBox();
            this.tbTuan4 = new System.Windows.Forms.TextBox();
            this.tbTuan3 = new System.Windows.Forms.TextBox();
            this.tbTuan2 = new System.Windows.Forms.TextBox();
            this.tbTuan1 = new System.Windows.Forms.TextBox();
            this.tbDiem45Phut = new System.Windows.Forms.TextBox();
            this.tbDiem15Phut = new System.Windows.Forms.TextBox();
            this.tbDiemMieng = new System.Windows.Forms.TextBox();
            this.tbTenHS = new System.Windows.Forms.TextBox();
            this.btThemDanhGia = new System.Windows.Forms.Button();
            this.btClose = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.cboMonHoc = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 4);
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(534, 50);
            this.label1.TabIndex = 0;
            this.label1.Text = "ĐÁNH GIÁ HỌC SINH";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 374);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nhận xét CMHS";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 347);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 27);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nhận xét GVCN";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 320);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 27);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nhận xét CBL";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(277, 266);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 27);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tuần 3";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(277, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 27);
            this.label6.TabIndex = 5;
            this.label6.Text = "Điểm 45 phút";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(277, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 27);
            this.label7.TabIndex = 6;
            this.label7.Text = "Điểm 15 phút";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(277, 131);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 27);
            this.label8.TabIndex = 7;
            this.label8.Text = "Điểm miệng";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(277, 293);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 27);
            this.label10.TabIndex = 9;
            this.label10.Text = "Tuần 4";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(277, 239);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 27);
            this.label11.TabIndex = 10;
            this.label11.Text = "Tuần 2";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(277, 212);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 27);
            this.label12.TabIndex = 11;
            this.label12.Text = "Tuần 1";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(3, 104);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 27);
            this.label13.TabIndex = 12;
            this.label13.Text = "Môn học";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(3, 77);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(110, 27);
            this.label15.TabIndex = 14;
            this.label15.Text = "Tên học sinh";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbMaHS
            // 
            this.lbMaHS.AutoSize = true;
            this.lbMaHS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMaHS.Location = new System.Drawing.Point(3, 50);
            this.lbMaHS.Name = "lbMaHS";
            this.lbMaHS.Size = new System.Drawing.Size(110, 27);
            this.lbMaHS.TabIndex = 15;
            this.lbMaHS.Text = "Mã học sinh";
            this.lbMaHS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbMaHS
            // 
            this.tbMaHS.BackColor = System.Drawing.SystemColors.Info;
            this.tableLayoutPanel1.SetColumnSpan(this.tbMaHS, 3);
            this.tbMaHS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMaHS.Location = new System.Drawing.Point(119, 52);
            this.tbMaHS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbMaHS.Name = "tbMaHS";
            this.tbMaHS.Size = new System.Drawing.Size(412, 23);
            this.tbMaHS.TabIndex = 16;
            // 
            // tbNhanXetCMHS
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tbNhanXetCMHS, 3);
            this.tbNhanXetCMHS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNhanXetCMHS.Location = new System.Drawing.Point(119, 376);
            this.tbNhanXetCMHS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbNhanXetCMHS.Name = "tbNhanXetCMHS";
            this.tbNhanXetCMHS.Size = new System.Drawing.Size(412, 23);
            this.tbNhanXetCMHS.TabIndex = 18;
            // 
            // tbNhanXetGVCN
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tbNhanXetGVCN, 3);
            this.tbNhanXetGVCN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNhanXetGVCN.Location = new System.Drawing.Point(119, 349);
            this.tbNhanXetGVCN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbNhanXetGVCN.Name = "tbNhanXetGVCN";
            this.tbNhanXetGVCN.Size = new System.Drawing.Size(412, 23);
            this.tbNhanXetGVCN.TabIndex = 19;
            // 
            // tbNhanXetCBL
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tbNhanXetCBL, 3);
            this.tbNhanXetCBL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbNhanXetCBL.Location = new System.Drawing.Point(119, 322);
            this.tbNhanXetCBL.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbNhanXetCBL.Name = "tbNhanXetCBL";
            this.tbNhanXetCBL.Size = new System.Drawing.Size(412, 23);
            this.tbNhanXetCBL.TabIndex = 20;
            // 
            // tbTuan4
            // 
            this.tbTuan4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTuan4.Location = new System.Drawing.Point(378, 295);
            this.tbTuan4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbTuan4.Name = "tbTuan4";
            this.tbTuan4.Size = new System.Drawing.Size(153, 23);
            this.tbTuan4.TabIndex = 21;
            // 
            // tbTuan3
            // 
            this.tbTuan3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTuan3.Location = new System.Drawing.Point(378, 268);
            this.tbTuan3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbTuan3.Name = "tbTuan3";
            this.tbTuan3.Size = new System.Drawing.Size(153, 23);
            this.tbTuan3.TabIndex = 22;
            // 
            // tbTuan2
            // 
            this.tbTuan2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTuan2.Location = new System.Drawing.Point(378, 241);
            this.tbTuan2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbTuan2.Name = "tbTuan2";
            this.tbTuan2.Size = new System.Drawing.Size(153, 23);
            this.tbTuan2.TabIndex = 23;
            // 
            // tbTuan1
            // 
            this.tbTuan1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTuan1.Location = new System.Drawing.Point(378, 214);
            this.tbTuan1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbTuan1.Name = "tbTuan1";
            this.tbTuan1.Size = new System.Drawing.Size(153, 23);
            this.tbTuan1.TabIndex = 24;
            // 
            // tbDiem45Phut
            // 
            this.tbDiem45Phut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDiem45Phut.Location = new System.Drawing.Point(378, 187);
            this.tbDiem45Phut.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDiem45Phut.Name = "tbDiem45Phut";
            this.tbDiem45Phut.Size = new System.Drawing.Size(153, 23);
            this.tbDiem45Phut.TabIndex = 25;
            // 
            // tbDiem15Phut
            // 
            this.tbDiem15Phut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDiem15Phut.Location = new System.Drawing.Point(378, 160);
            this.tbDiem15Phut.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDiem15Phut.Name = "tbDiem15Phut";
            this.tbDiem15Phut.Size = new System.Drawing.Size(153, 23);
            this.tbDiem15Phut.TabIndex = 26;
            // 
            // tbDiemMieng
            // 
            this.tbDiemMieng.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDiemMieng.Location = new System.Drawing.Point(378, 133);
            this.tbDiemMieng.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDiemMieng.Name = "tbDiemMieng";
            this.tbDiemMieng.Size = new System.Drawing.Size(153, 23);
            this.tbDiemMieng.TabIndex = 27;
            // 
            // tbTenHS
            // 
            this.tbTenHS.BackColor = System.Drawing.SystemColors.Info;
            this.tableLayoutPanel1.SetColumnSpan(this.tbTenHS, 3);
            this.tbTenHS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTenHS.Location = new System.Drawing.Point(119, 79);
            this.tbTenHS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbTenHS.Name = "tbTenHS";
            this.tbTenHS.Size = new System.Drawing.Size(412, 23);
            this.tbTenHS.TabIndex = 31;
            // 
            // btThemDanhGia
            // 
            this.btThemDanhGia.Location = new System.Drawing.Point(335, 2);
            this.btThemDanhGia.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btThemDanhGia.Name = "btThemDanhGia";
            this.btThemDanhGia.Size = new System.Drawing.Size(115, 28);
            this.btThemDanhGia.TabIndex = 32;
            this.btThemDanhGia.Text = "Thêm đánh giá";
            this.btThemDanhGia.UseVisualStyleBackColor = true;
            this.btThemDanhGia.Click += new System.EventHandler(this.btThemDanhGia_Click);
            // 
            // btClose
            // 
            this.btClose.Location = new System.Drawing.Point(456, 2);
            this.btClose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(75, 28);
            this.btClose.TabIndex = 33;
            this.btClose.Text = "Thoát";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(277, 104);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 27);
            this.label9.TabIndex = 34;
            this.label9.Text = "Ngày:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(378, 106);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(153, 23);
            this.dateTimePicker1.TabIndex = 35;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dateTimePicker1, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbMaHS, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbNhanXetCBL, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.tbNhanXetCMHS, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.tbNhanXetGVCN, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.tbTuan4, 3, 10);
            this.tableLayoutPanel1.Controls.Add(this.tbTuan3, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.tbTuan2, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.tbTuan1, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.tbDiem45Phut, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.label9, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbTenHS, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbMaHS, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label10, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.label11, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.label5, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.label12, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.tbDiemMieng, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbDiem15Phut, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 16);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 15);
            this.tableLayoutPanel1.Controls.Add(this.cboMonHoc, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 17;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(534, 462);
            this.tableLayoutPanel1.TabIndex = 36;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 4);
            this.flowLayoutPanel1.Controls.Add(this.btClose);
            this.flowLayoutPanel1.Controls.Add(this.btThemDanhGia);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 430);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(534, 32);
            this.flowLayoutPanel1.TabIndex = 36;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel1.SetColumnSpan(this.label14, 4);
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(3, 428);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(528, 2);
            this.label14.TabIndex = 37;
            // 
            // cboMonHoc
            // 
            this.cboMonHoc.FormattingEnabled = true;
            this.cboMonHoc.Items.AddRange(new object[] {
            "Toán",
            "Văn",
            "Anh",
            "Lý",
            "Hóa",
            "Địa",
            "Sủ",
            "GDCD"});
            this.cboMonHoc.Location = new System.Drawing.Point(119, 107);
            this.cboMonHoc.Name = "cboMonHoc";
            this.cboMonHoc.Size = new System.Drawing.Size(152, 24);
            this.cboMonHoc.TabIndex = 38;
            // 
            // fDanhGiaHocSinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 462);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(550, 500);
            this.Name = "fDanhGiaHocSinh";
            this.Text = "fDanhGiaHocSinh";
            this.Load += new System.EventHandler(this.fDanhGiaHocSinh_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbMaHS;
        private System.Windows.Forms.TextBox tbMaHS;
        private System.Windows.Forms.TextBox tbNhanXetCMHS;
        private System.Windows.Forms.TextBox tbNhanXetGVCN;
        private System.Windows.Forms.TextBox tbNhanXetCBL;
        private System.Windows.Forms.TextBox tbTuan4;
        private System.Windows.Forms.TextBox tbTuan3;
        private System.Windows.Forms.TextBox tbTuan2;
        private System.Windows.Forms.TextBox tbTuan1;
        private System.Windows.Forms.TextBox tbDiem45Phut;
        private System.Windows.Forms.TextBox tbDiem15Phut;
        private System.Windows.Forms.TextBox tbDiemMieng;
        private System.Windows.Forms.TextBox tbTenHS;
        private System.Windows.Forms.Button btThemDanhGia;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cboMonHoc;
    }
}