﻿namespace PhanMemQuanLiHocSinh
{
    partial class fDanhMucHocSinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTille = new System.Windows.Forms.Label();
            this.lbMHS = new System.Windows.Forms.Label();
            this.lbStudentName = new System.Windows.Forms.Label();
            this.lbGioiTinh = new System.Windows.Forms.Label();
            this.lbDateOfBirth = new System.Windows.Forms.Label();
            this.lbDanToc = new System.Windows.Forms.Label();
            this.tbMaHocSinh = new System.Windows.Forms.TextBox();
            this.tbStudentName = new System.Windows.Forms.TextBox();
            this.tbDanToc = new System.Windows.Forms.TextBox();
            this.btAddStudent = new System.Windows.Forms.Button();
            this.dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.btnReturnToMenu = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.optNam = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbTille
            // 
            this.lbTille.AutoSize = true;
            this.lbTille.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.tableLayoutPanel1.SetColumnSpan(this.lbTille, 2);
            this.lbTille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTille.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTille.ForeColor = System.Drawing.Color.White;
            this.lbTille.Location = new System.Drawing.Point(0, 0);
            this.lbTille.Margin = new System.Windows.Forms.Padding(0);
            this.lbTille.Name = "lbTille";
            this.lbTille.Size = new System.Drawing.Size(484, 50);
            this.lbTille.TabIndex = 3;
            this.lbTille.Text = "DANH MỤC HỌC SINH";
            this.lbTille.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbMHS
            // 
            this.lbMHS.AutoSize = true;
            this.lbMHS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMHS.ForeColor = System.Drawing.Color.Red;
            this.lbMHS.Location = new System.Drawing.Point(3, 50);
            this.lbMHS.Name = "lbMHS";
            this.lbMHS.Size = new System.Drawing.Size(93, 27);
            this.lbMHS.TabIndex = 4;
            this.lbMHS.Text = "Mã học sinh";
            this.lbMHS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbStudentName
            // 
            this.lbStudentName.AutoSize = true;
            this.lbStudentName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbStudentName.Location = new System.Drawing.Point(3, 77);
            this.lbStudentName.Name = "lbStudentName";
            this.lbStudentName.Size = new System.Drawing.Size(93, 27);
            this.lbStudentName.TabIndex = 5;
            this.lbStudentName.Text = "Tên học sinh";
            this.lbStudentName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbGioiTinh
            // 
            this.lbGioiTinh.AutoSize = true;
            this.lbGioiTinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbGioiTinh.Location = new System.Drawing.Point(3, 104);
            this.lbGioiTinh.Name = "lbGioiTinh";
            this.lbGioiTinh.Size = new System.Drawing.Size(93, 26);
            this.lbGioiTinh.TabIndex = 6;
            this.lbGioiTinh.Text = "Giới tính";
            this.lbGioiTinh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDateOfBirth
            // 
            this.lbDateOfBirth.AutoSize = true;
            this.lbDateOfBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDateOfBirth.Location = new System.Drawing.Point(3, 130);
            this.lbDateOfBirth.Name = "lbDateOfBirth";
            this.lbDateOfBirth.Size = new System.Drawing.Size(93, 27);
            this.lbDateOfBirth.TabIndex = 7;
            this.lbDateOfBirth.Text = "Ngày sinh";
            this.lbDateOfBirth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDanToc
            // 
            this.lbDanToc.AutoSize = true;
            this.lbDanToc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDanToc.Location = new System.Drawing.Point(3, 157);
            this.lbDanToc.Name = "lbDanToc";
            this.lbDanToc.Size = new System.Drawing.Size(93, 27);
            this.lbDanToc.TabIndex = 8;
            this.lbDanToc.Text = "Dân Tộc";
            this.lbDanToc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbMaHocSinh
            // 
            this.tbMaHocSinh.BackColor = System.Drawing.SystemColors.Info;
            this.tbMaHocSinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMaHocSinh.Location = new System.Drawing.Point(102, 52);
            this.tbMaHocSinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbMaHocSinh.Name = "tbMaHocSinh";
            this.tbMaHocSinh.ReadOnly = true;
            this.tbMaHocSinh.Size = new System.Drawing.Size(379, 23);
            this.tbMaHocSinh.TabIndex = 0;
            // 
            // tbStudentName
            // 
            this.tbStudentName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbStudentName.Location = new System.Drawing.Point(102, 79);
            this.tbStudentName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbStudentName.Name = "tbStudentName";
            this.tbStudentName.Size = new System.Drawing.Size(379, 23);
            this.tbStudentName.TabIndex = 10;
            // 
            // tbDanToc
            // 
            this.tbDanToc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDanToc.Location = new System.Drawing.Point(102, 159);
            this.tbDanToc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDanToc.Name = "tbDanToc";
            this.tbDanToc.Size = new System.Drawing.Size(379, 23);
            this.tbDanToc.TabIndex = 40;
            // 
            // btAddStudent
            // 
            this.btAddStudent.Location = new System.Drawing.Point(258, 2);
            this.btAddStudent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAddStudent.Name = "btAddStudent";
            this.btAddStudent.Size = new System.Drawing.Size(122, 28);
            this.btAddStudent.TabIndex = 0;
            this.btAddStudent.Text = "Thêm học sinh";
            this.btAddStudent.UseVisualStyleBackColor = true;
            this.btAddStudent.Click += new System.EventHandler(this.btAddStudent_Click);
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.CustomFormat = "dd/MM/yyyy";
            this.dtpNgaySinh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh.Location = new System.Drawing.Point(102, 132);
            this.dtpNgaySinh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Size = new System.Drawing.Size(379, 23);
            this.dtpNgaySinh.TabIndex = 30;
            // 
            // btnReturnToMenu
            // 
            this.btnReturnToMenu.Location = new System.Drawing.Point(386, 2);
            this.btnReturnToMenu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnReturnToMenu.Name = "btnReturnToMenu";
            this.btnReturnToMenu.Size = new System.Drawing.Size(89, 28);
            this.btnReturnToMenu.TabIndex = 10;
            this.btnReturnToMenu.Text = "Exit";
            this.btnReturnToMenu.UseVisualStyleBackColor = true;
            this.btnReturnToMenu.Click += new System.EventHandler(this.btnReturnToMenu_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lbTille, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbDanToc, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.dtpNgaySinh, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lbMHS, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbStudentName, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbGioiTinh, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbStudentName, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbDateOfBirth, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbMaHocSinh, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbDanToc, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(484, 262);
            this.tableLayoutPanel1.TabIndex = 29;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.btnReturnToMenu);
            this.flowLayoutPanel1.Controls.Add(this.btAddStudent);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 227);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(478, 32);
            this.flowLayoutPanel1.TabIndex = 50;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.Controls.Add(this.optNam);
            this.flowLayoutPanel2.Controls.Add(this.radioButton2);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(99, 104);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(385, 26);
            this.flowLayoutPanel2.TabIndex = 51;
            this.flowLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel2_Paint);
            // 
            // optNam
            // 
            this.optNam.AutoSize = true;
            this.optNam.Location = new System.Drawing.Point(3, 3);
            this.optNam.Name = "optNam";
            this.optNam.Size = new System.Drawing.Size(54, 20);
            this.optNam.TabIndex = 0;
            this.optNam.TabStop = true;
            this.optNam.Text = "Nam";
            this.optNam.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(63, 3);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(44, 20);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Nữ";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // fDanhMucHocSinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 262);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(500, 300);
            this.Name = "fDanhMucHocSinh";
            this.Text = "Phần mềm quản lí học sinh";
            this.Load += new System.EventHandler(this.fDanhMucHocSinh_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbTille;
        private System.Windows.Forms.Label lbMHS;
        private System.Windows.Forms.Label lbStudentName;
        private System.Windows.Forms.Label lbGioiTinh;
        private System.Windows.Forms.Label lbDateOfBirth;
        private System.Windows.Forms.Label lbDanToc;
        private System.Windows.Forms.TextBox tbMaHocSinh;
        private System.Windows.Forms.TextBox tbStudentName;
        private System.Windows.Forms.TextBox tbDanToc;
        private System.Windows.Forms.Button btAddStudent;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh;
        private System.Windows.Forms.Button btnReturnToMenu;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.RadioButton optNam;
        private System.Windows.Forms.RadioButton radioButton2;
    }
}