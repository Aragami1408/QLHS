﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDanhMucMonHoc : Form
    {
        public string MaMonHoc { get; set; }

        public fDanhMucMonHoc()
        {
            InitializeComponent();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btBackToMenu_Click(object sender, EventArgs e)
        {
            Hide();
            fDangNhap f = new fDangNhap();
            f.Show();
        }

        private void btThemMonHoc_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi);
            conn.Open();
            if (String.IsNullOrEmpty(tbMaMonHoc.Text))
            {
                MessageBox.Show("Mời bạn nhập mã môn học", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbMonHoc.Text))
            {
                MessageBox.Show("Mời bạn nhập môn học", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbTeacher.Text))
            {
                MessageBox.Show("Mời bạn nhập tên giáo viên", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                try
                {
                    string queryInsert = "INSERT INTO DMMonHoc(MaMH, TenMH, GiaoVienDay) VALUES (@MaMH, @TenMH, @GiaoVienDay)";
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    if (string.IsNullOrEmpty(this.MaMonHoc))
                        cmd.CommandText = queryInsert;
                    else
                        cmd.CommandText = "UPDATE DMMonHoc set MaMH = @MaMH, TenMH = @TenMH, GiaoVienDay = @GiaoVienDay where MaMH = '" + MaMonHoc + "'";

                    cmd.Parameters.AddWithValue("@MaMH", tbMaMonHoc.Text);
                    cmd.Parameters.AddWithValue("@TenMH", tbMonHoc.Text);
                    cmd.Parameters.AddWithValue("@GiaoVienDay", tbTeacher.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Bạn đã thêm môn học thành công", "ADDING DATA IS COMPLETE!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    conn.Close();
                    conn.Dispose();


                }
                catch (SqlException sqlexp)
                {
                    MessageBox.Show(sqlexp.Message, "ERROR FOUND!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw;
                }

            }

        }

        private void fDanhMucMonHoc_Load(object sender, EventArgs e)
        {
            string id = Guid.NewGuid().ToString();
            tbMaMonHoc.Text = id;
            if (!String.IsNullOrEmpty(this.MaMonHoc))
            {
                SqlConnection con = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi);
                con.Open();
                SqlCommand cmd = new SqlCommand("Select * FROM DMMonHoc WHERE MaMH ='" + this.MaMonHoc+ "'");
                cmd.Connection = con;
                SqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    tbMaMonHoc.Text = rd["MaMH"].ToString();
                    tbMonHoc.Text = rd["TenMH"].ToString();
                    tbTeacher.Text = rd["GiaoVienDay"].ToString();
                }
                rd.Close();
                con.Close();
                con.Dispose();
            }

        }
    }
}
