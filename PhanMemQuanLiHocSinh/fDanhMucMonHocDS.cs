﻿using QLHSSupporter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDanhMucMonHocDS : Form
    {
        public fDanhMucMonHocDS()
        {
            InitializeComponent();
        }

        private void fDanhMucMonHocDS_Load(object sender, EventArgs e)
        {
            loadData();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            fDanhMucMonHoc fDMMH = new fDanhMucMonHoc();
            fDMMH.Show(this);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            loadData();
        }
        private void loadData()
        {
            try
            {
                
                dataGridView1.Rows.Clear();

                
                using (DatabaseHelper db = new DatabaseHelper())
                {
                    SqlDataReader rd = db.ExecuteReader("SELECT * FROM DMMonHoc");

                    while (rd.Read())
                        dataGridView1.Rows.Add(rd["MaMH"].ToString(), rd["TenMH"].ToString(), rd["GiaoVienDay"].ToString());

                    //dong ket noi
                    rd.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex > -1)
                {
                    fDanhMucMonHoc form = new PhanMemQuanLiHocSinh.fDanhMucMonHoc();
                    form.MaMonHoc = dataGridView1.Rows[e.RowIndex].Cells["colMaMonHoc"].Value.ToString();
                    form.Show(this);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                throw;
            }
            
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            deleteData();
        }

        private void deleteData()
        {
            if (dataGridView1.CurrentRow != null)
            {

                SqlConnection con = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi);
                DialogResult result = MessageBox.Show("Bạn có muốn xóa không", "WARNING!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    con.Open();
                    string currentMaMonHoc = dataGridView1.CurrentRow.Cells["colMaMonHoc"].Value.ToString();

                    string deleteQuery = "DELETE FROM DMMonHoc WHERE MaMH = @currentMaMonHoc";
                    SqlCommand cmd = new SqlCommand(deleteQuery, con);
                    cmd.Parameters.AddWithValue("@currentMaMonHoc", currentMaMonHoc);
                    cmd.ExecuteNonQuery();

                    dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                }

            }
        }

        private void refreshToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            loadData();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        
    }
}
