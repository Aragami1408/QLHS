﻿using Phần_mềm_quản_lý_học_sinh.Form_folder.MainForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDanhSachHocSinh : Form
    {
        public fDanhSachHocSinh()
        {
            InitializeComponent();
        }

        private void tsbThemHocSinh_Click(object sender, EventArgs e)
        {
            fSYLL f = new fSYLL();
            f.Show();
        }

        private void tsbRefresh_Click(object sender, EventArgs e)
        {
            loadData();
        }
        void loadData()
        {
            try
            {
                dataGridView1.Rows.Clear();

                SqlConnection cnn = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi);
                cnn.Open();

                SqlCommand cmd = new SqlCommand(string.Format("SELECT * FROM SYLL"),cnn);
                

                SqlDataReader rd = cmd.ExecuteReader();

                while (rd.Read())
                {
                    dataGridView1.Rows.Add(rd["MaHS"].ToString(),
                        rd["firstName"].ToString(),
                        rd["LastName"].ToString(),
                        rd["GioiTinh"].ToString(),
                        rd["NgaySinh"],
                        rd["NoiSinh"].ToString(),
                        rd["DanToc"].ToString(),
                        rd["NoiO"].ToString(),
                        rd["UuTien"].ToString(),
                        rd["DienThoai"].ToString(),
                        rd["NhiemVu"].ToString()
                        )
                        ;
                }

                //dong ket noi
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void fDanhSachHocSinh_Load(object sender, EventArgs e)
        {
            loadData();
        }

        private void tsbXoa_Click(object sender, EventArgs e)
        {
            DeleteData();
        }
        void DeleteData()
        {
            SqlConnection con = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi);
            DialogResult result = MessageBox.Show("Bạn có muốn xóa không", "WARNING!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                con.Open();
                string currentMaHocSinh = dataGridView1.CurrentRow.Cells["colMaHS"].Value.ToString();

                string deleteQuery = "DELETE FROM SYLL WHERE MaHS = @currentMaHocSinh";
                SqlCommand cmd = new SqlCommand(deleteQuery, con);
                cmd.Parameters.AddWithValue("@currentMaHocSinh", currentMaHocSinh);
                cmd.ExecuteNonQuery();

                dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                con.Close();
                con.Dispose();

            }
        }

        private void sửaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void xóaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteData();
        }

        private void nạpLạiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadData();
        }

        
    }
}
