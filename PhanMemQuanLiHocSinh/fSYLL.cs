﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Phần_mềm_quản_lý_học_sinh.Form_folder.MainForm
{
    public partial class fSYLL : Form
    {

        public fSYLL()
        {

            InitializeComponent();
        }
        private void btAddSYLL_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi); 
            if (String.IsNullOrEmpty(cHSFirstNameTextBox.Text))
            {
                MessageBox.Show("Mời bạn nhập họ của học sinh", "QLHS", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cHSFirstNameTextBox.Focus();
            }
            else if (String.IsNullOrEmpty(tblastNameHS.Text))
            {
                MessageBox.Show("Mời bạn nhập tên học sinh", "QLHS", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tblastNameHS.Focus();
            }
            else if (!rdNam.Checked & !rdNu.Checked)
            {
                MessageBox.Show("Mời bạn chon giới tính", "QLHS");
                rdNam.Focus();
            }
            else if (String.IsNullOrEmpty(cNoiOTextBox.Text))
            {
                MessageBox.Show("Mời bạn nhập nơi ở của học sinh", "QLHS", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cNoiOTextBox.Focus();
            }
            else if (String.IsNullOrEmpty(tbDienThoai.Text))
            {
                MessageBox.Show("Mời bạn nhập số điện thoại của học sinh hoặc của phụ huynh", "QLHS", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                tbDienThoai.Focus();
            }
            else
            {
                try
                {
                    con.Open();
                    string insert = @"insert into SYLL(MaHS, firstName,LastName, GioiTinh, NgaySinh, NoiSinh, DanToc, NoiO, UuTien,DienThoai,NhiemVu) values(@MaHS, @firstName,@LastName, @GioiTinh, @NgaySinh, @NoiSinh, @DanToc, @NoiO, @UuTien,@DienThoai,@NhiemVu)";
                   
                    SqlCommand cmd = new SqlCommand(insert, con);

                    cmd.Parameters.AddWithValue("@MaHS", cMaHSTextBox.Text);
                    cmd.Parameters.AddWithValue("@firstName", cHSFirstNameTextBox.Text);
                    cmd.Parameters.AddWithValue("@LastName", tblastNameHS.Text);
                    cmd.Parameters.AddWithValue("@GioiTinh", rdNam.Checked? "Nam" : "Nữ");
                    cmd.Parameters.AddWithValue("@NgaySinh", dNgaySinhDateTimePicker.Value);
                    cmd.Parameters.AddWithValue("@NoiSinh", cNoiSinhTextBox.Text);
                    cmd.Parameters.AddWithValue("@DanToc", cDanTocTextBox.Text);
                    cmd.Parameters.AddWithValue("@NoiO", cNoiOTextBox.Text);
                    cmd.Parameters.AddWithValue("@UuTien", cUutienTextBox.Text);
                    cmd.Parameters.AddWithValue("@DienThoai", tbDienThoai.Text);
                    cmd.Parameters.AddWithValue("@NhiemVu", tbNhiemVu.Text);
                    cmd.ExecuteNonQuery();
                    con.Close();
                    con.Dispose();
                    MessageBox.Show("Thêm học sinh thành công!!", "QLHS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (SqlException SE)
                {
                    MessageBox.Show(SE.Message);
                }
                
            }
        }

        private void fSYLL_Load(object sender, EventArgs e)
        {
            string id = Guid.NewGuid().ToString();
            cMaHSTextBox.Text = id;
        }
    }
}