﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Phần_mềm_quản_lý_học_sinh.Form_folder.MainForm;
using PhanMemQuanLiHocSinh;

namespace Phần_mềm_quản_lý_học_sinh
{
    public partial class F_Main : Form
    {
        public F_Main()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            fSYLL fSYLL = new fSYLL();
            fSYLL.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fDanhSachHocSinh f = new fDanhSachHocSinh();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            fDanhMucMonHocDS f = new fDanhMucMonHocDS();
            f.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            fTTTL f = new fTTTL();
            f.Show();
        }

        private void btSignOut_Click(object sender, EventArgs e)
        {
            Hide();
            fDangNhap f = new fDangNhap();
            f.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Close();
            Dispose();

            //thoát hoàn toàn
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            fDMDiemThiDua f = new fDMDiemThiDua();
            f.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Tính năng hiện đang được nâng cấp, xin lỗi về sự bất tiện này", "SORRY!!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            fDanhGiaHocSinh f = new fDanhGiaHocSinh();
            f.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            fDMCanBoLop f = new fDMCanBoLop();
            f.Show();
        }

        private void F_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}