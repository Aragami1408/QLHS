﻿using Phần_mềm_quản_lý_học_sinh;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    static class Program
    {
        //public static string ChuoiKetNoi = @"server=.\SQLEXPRESS;database=QLHS;uid=sa;password=sa123";
        //public static string ChuoiKetNoi;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            QLHSSupporter.DatabaseHelper.ChuoiKetNoi = string.Empty;

            Application.Run(new F_Main());
            //Application.Run(new fDangNhap());
        }
    }
}
