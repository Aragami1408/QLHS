﻿namespace PhanMemQuanLiHocSinh
{
    partial class fDMCanBoLop
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTieuDe = new System.Windows.Forms.Label();
            this.lbMaHS = new System.Windows.Forms.Label();
            this.lbTenHocSinh = new System.Windows.Forms.Label();
            this.lbChucVu = new System.Windows.Forms.Label();
            this.lbDienThoai = new System.Windows.Forms.Label();
            this.tbMaHS = new System.Windows.Forms.TextBox();
            this.tbNhiemVu = new System.Windows.Forms.TextBox();
            this.tbDienThoai = new System.Windows.Forms.TextBox();
            this.tbTenHS = new System.Windows.Forms.TextBox();
            this.btThenCanBo = new System.Windows.Forms.Button();
            this.btClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbTieuDe
            // 
            this.lbTieuDe.AutoSize = true;
            this.lbTieuDe.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTieuDe.ForeColor = System.Drawing.Color.Red;
            this.lbTieuDe.Location = new System.Drawing.Point(48, 22);
            this.lbTieuDe.Name = "lbTieuDe";
            this.lbTieuDe.Size = new System.Drawing.Size(379, 35);
            this.lbTieuDe.TabIndex = 0;
            this.lbTieuDe.Text = "DANH MỤC CÁN BỘ LỚP";
            // 
            // lbMaHS
            // 
            this.lbMaHS.AutoSize = true;
            this.lbMaHS.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMaHS.Location = new System.Drawing.Point(116, 91);
            this.lbMaHS.Name = "lbMaHS";
            this.lbMaHS.Size = new System.Drawing.Size(106, 22);
            this.lbMaHS.TabIndex = 1;
            this.lbMaHS.Text = "Mã học sinh";
            // 
            // lbTenHocSinh
            // 
            this.lbTenHocSinh.AutoSize = true;
            this.lbTenHocSinh.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenHocSinh.Location = new System.Drawing.Point(116, 140);
            this.lbTenHocSinh.Name = "lbTenHocSinh";
            this.lbTenHocSinh.Size = new System.Drawing.Size(110, 22);
            this.lbTenHocSinh.TabIndex = 2;
            this.lbTenHocSinh.Text = "Tên học sinh";
            // 
            // lbChucVu
            // 
            this.lbChucVu.AutoSize = true;
            this.lbChucVu.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChucVu.Location = new System.Drawing.Point(116, 191);
            this.lbChucVu.Name = "lbChucVu";
            this.lbChucVu.Size = new System.Drawing.Size(86, 22);
            this.lbChucVu.TabIndex = 3;
            this.lbChucVu.Text = "Nhiệm vụ";
            // 
            // lbDienThoai
            // 
            this.lbDienThoai.AutoSize = true;
            this.lbDienThoai.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDienThoai.Location = new System.Drawing.Point(116, 241);
            this.lbDienThoai.Name = "lbDienThoai";
            this.lbDienThoai.Size = new System.Drawing.Size(92, 22);
            this.lbDienThoai.TabIndex = 4;
            this.lbDienThoai.Text = "Điện thoại";
            // 
            // tbMaHS
            // 
            this.tbMaHS.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMaHS.Location = new System.Drawing.Point(284, 90);
            this.tbMaHS.Name = "tbMaHS";
            this.tbMaHS.Size = new System.Drawing.Size(100, 22);
            this.tbMaHS.TabIndex = 5;
            // 
            // tbNhiemVu
            // 
            this.tbNhiemVu.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNhiemVu.Location = new System.Drawing.Point(284, 191);
            this.tbNhiemVu.Name = "tbNhiemVu";
            this.tbNhiemVu.Size = new System.Drawing.Size(100, 22);
            this.tbNhiemVu.TabIndex = 6;
            // 
            // tbDienThoai
            // 
            this.tbDienThoai.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDienThoai.Location = new System.Drawing.Point(284, 241);
            this.tbDienThoai.Name = "tbDienThoai";
            this.tbDienThoai.Size = new System.Drawing.Size(100, 22);
            this.tbDienThoai.TabIndex = 7;
            // 
            // tbTenHS
            // 
            this.tbTenHS.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTenHS.Location = new System.Drawing.Point(284, 140);
            this.tbTenHS.Name = "tbTenHS";
            this.tbTenHS.Size = new System.Drawing.Size(100, 22);
            this.tbTenHS.TabIndex = 8;
            // 
            // btThenCanBo
            // 
            this.btThenCanBo.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btThenCanBo.Location = new System.Drawing.Point(284, 289);
            this.btThenCanBo.Name = "btThenCanBo";
            this.btThenCanBo.Size = new System.Drawing.Size(91, 52);
            this.btThenCanBo.TabIndex = 9;
            this.btThenCanBo.Text = "Thêm cán bộ lớp";
            this.btThenCanBo.UseVisualStyleBackColor = true;
            this.btThenCanBo.Click += new System.EventHandler(this.btThenCanBo_Click);
            // 
            // btClose
            // 
            this.btClose.Font = new System.Drawing.Font("Times New Roman", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Location = new System.Drawing.Point(414, 289);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(91, 51);
            this.btClose.TabIndex = 10;
            this.btClose.Text = "Thoát khỏi chương trinh";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // fDMCanBoLop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 354);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.btThenCanBo);
            this.Controls.Add(this.tbTenHS);
            this.Controls.Add(this.tbDienThoai);
            this.Controls.Add(this.tbNhiemVu);
            this.Controls.Add(this.tbMaHS);
            this.Controls.Add(this.lbDienThoai);
            this.Controls.Add(this.lbChucVu);
            this.Controls.Add(this.lbTenHocSinh);
            this.Controls.Add(this.lbMaHS);
            this.Controls.Add(this.lbTieuDe);
            this.Name = "fDMCanBoLop";
            this.Text = "fDMCanBoLop";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTieuDe;
        private System.Windows.Forms.Label lbMaHS;
        private System.Windows.Forms.Label lbTenHocSinh;
        private System.Windows.Forms.Label lbChucVu;
        private System.Windows.Forms.Label lbDienThoai;
        private System.Windows.Forms.TextBox tbMaHS;
        private System.Windows.Forms.TextBox tbNhiemVu;
        private System.Windows.Forms.TextBox tbDienThoai;
        private System.Windows.Forms.TextBox tbTenHS;
        private System.Windows.Forms.Button btThenCanBo;
        private System.Windows.Forms.Button btClose;
    }
}