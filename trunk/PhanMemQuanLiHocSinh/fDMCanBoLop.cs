﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDMCanBoLop : Form
    {
        public fDMCanBoLop()
        {
            InitializeComponent();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
            Dispose();
        }

        private void btThenCanBo_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"server=.\SQLEXPRESS;database=QLHS;uid=sa;password=sa123");
            if (String.IsNullOrWhiteSpace(tbMaHS.Text))
            {
                MessageBox.Show("Mời bạn nhập lại mã học sinh", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbTenHS.Text))
            {
                MessageBox.Show("Mời bạn nhập lai tên học sinh", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbNhiemVu.Text))
            {
                MessageBox.Show("Mời bạn nhập lại nhiệm vụ của cán bộ lớp", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbDienThoai.Text) || tbDienThoai.Text.Length > 11)
            {
                MessageBox.Show("Mời bạn nhập lại số điện thoại", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string insert = "INSERT INTO tCBL (cMaHS, cTenHS, cNhiemVu, cDienThoai) VALUES (@cMaHS, @cTenHS, @cNhiemVu, @cDienThoai)";
                try
                {
                    SqlCommand cmd = new SqlCommand(insert, conn);
                    cmd.Parameters.AddWithValue("@cMaHS", tbMaHS.Text);
                    cmd.Parameters.AddWithValue("@cTenHS", tbTenHS.Text);
                    cmd.Parameters.AddWithValue("@cNhiemVu", tbNhiemVu.Text);
                    cmd.Parameters.AddWithValue("@cDienThoai", tbDienThoai.Text);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
                MessageBox.Show("Thêm cán bộ lớp thành công", "ADDING DATA IS COMPLETE!!",MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

            }
        }
    }
}
