﻿using Phần_mềm_quản_lý_học_sinh;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDMDiemThiDua : Form
    {
        public fDMDiemThiDua()
        {
            InitializeComponent();
        }

        private void btSignOut_Click(object sender, EventArgs e)
        {
            Hide();
            fDangNhap f = new fDangNhap();
            f.Show();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
            Dispose();
        }

        private void btLuuVaTinhDiemThiDua_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"server=.\SQLEXPRESS;database=QLHS;uid=sa;password=sa123");
            conn.Open();
            if (String.IsNullOrWhiteSpace(tbMaHS.Text))
            {
                MessageBox.Show("Mời bạn nhập lại mã học sinh", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbTenHocSinh.Text))
            {
                MessageBox.Show("Mời bạn nhập lại tên học sinh", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbNoiDungThiDua.Text))
            {
                MessageBox.Show("Mời bạn nhập nội dung thi đua và đánh giá", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbDiemCong.Text))
            {
                MessageBox.Show("Mời bạn nhập điểm cộng", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrWhiteSpace(tbDiemTru.Text))
            {
                MessageBox.Show("Mời bạn nhập điểm trừ", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                int TongDiem = 100 + int.Parse(tbDiemCong.Text) - int.Parse(tbDiemTru.Text);
                try
                {
                    string query = "INSERT INTO tDMThiDua (cMaHS, cTenHS, cNoiDung, nDiemCong, nDiemTru, nTongDiem) VALUES (@cMaHS, @cTenHS, @cNoiDung, @nDiemCong, @nDiemTru, @nTongDiem)";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@cMaHS", tbMaHS.Text);
                    cmd.Parameters.AddWithValue("@cTenHS", tbTenHocSinh.Text);
                    cmd.Parameters.AddWithValue("@cNoiDung", tbNoiDungThiDua.Text);
                    cmd.Parameters.AddWithValue("@nDiemCong", tbDiemCong.Text);
                    cmd.Parameters.AddWithValue("@nDiemTru", tbDiemTru.Text);
                    cmd.Parameters.AddWithValue("@nTongDiem", TongDiem);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }
                MessageBox.Show("Điểm thi đua của bạn là "+ TongDiem, "Phần mềm quản lý học sinh", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
    }
}
