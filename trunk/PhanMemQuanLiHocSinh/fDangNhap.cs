﻿using Phần_mềm_quản_lý_học_sinh;
using Phần_mềm_quản_lý_học_sinh.Form_folder.MainForm;
using QLHSSupporter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDangNhap : Form
    {
        public fDangNhap()
        {
            InitializeComponent();
        }
        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtTenTK.Text))
            {
                MessageBox.Show("Xin vui lòng nhập tài khoản!!!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //đặt con trỏ vào ô nhập thông tin tài khoản
                txtTenTK.Focus();
                return;
            }

            //kiểm tra thông tin đăng nhập vào máy chủ
            if (String.IsNullOrEmpty(tbServer.Text))
            {
                MessageBox.Show("Xin vui lòng nhập tên máy chủ!!!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //đặt con trỏ vào ô nhập thông tin tài khoản
                tbServer.Focus();
                return;
            }


            try
            {
                IniFile ini = new IniFile("user.ini");

                //tạo chuỗi kết nối dựa vào dữ liệu người dùng đã nhập
                //Program.ChuoiKetNoi = @"server=.\SQLEXPRESS;database=QLHS;uid=sa;password=sa123";                
                QLHSSupporter.DatabaseHelper.ChuoiKetNoi = string.Format("server={0};database=QLHS;uid={1};password={2}", tbServer.Text.Trim(), tbDBName.Text.Trim(), tbDBPass.Text.Trim());

                SqlConnection cnn = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi);
                cnn.Open();

                SqlCommand cmd = new SqlCommand(string.Format("SELECT [USER NAME] FROM Account WHERE [USER NAME] = '{0}' AND [PASS WORD]= '{1}'", txtTenTK.Text.Trim(), txtMatKhau.Text));
                cmd.Connection = cnn;

                SqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    if (rd["[USER NAME]"].ToString().Trim() == txtTenTK.Text.Trim())
                    {


                        //luu lai thong tin nguoi dung
                        if (chkGhiNhoTK.Checked)
                            ini.Write("user_name", txtTenTK.Text.Trim(), "Section1");

                        if (chkGhiNhoTKVaMK.Checked)
                        {
                            ini.Write("user_namee", txtTenTK.Text.Trim(), "Section1");
                            ini.Write("password", txtMatKhau.Text, "Section1");
                        }

                        ini.Write("HideOrVisible", tbDBName.Visible ? "1" : "0", "Section1");
                        ini.Write("ServerName", IniFile.Encrypt(tbServer.Text), "Section1");
                        ini.Write("DBName", tbDBName.Text, "Section1");
                        ini.Write("DBPass", IniFile.Encrypt(tbDBPass.Text), "Section1");
                        ini.Write("ghi_nho_nguoi_dung", chkGhiNhoTK.Checked ? "1" : "0", "Section1");
                        ini.Write("nghi_nho_nd_va_mk", chkGhiNhoTKVaMK.Checked ? "1" : "0", "Section1");

                        this.Hide();
                        F_Main f = new F_Main();
                        f.Show();

                    }
                    else
                        MessageBox.Show("Tên tài khoản hoặc mật khẩu không chính xác!!!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Tên tài khoản hoặc mật khẩu không chính xác!!!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Found!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void fDangNhap_Load(object sender, EventArgs e)
        {

            IniFile ini = new IniFile("user.ini");
            string HideOrUnHide = ini.Read("HideOrVisible", "Section1");
            if (HideOrUnHide == "1")
                tbDBName.Visible = false;
            button1_Click(sender, e);


            tbServer.Text = IniFile.Decrypt(ini.Read("ServerName", "Section1"));
            tbDBPass.Text = IniFile.Decrypt(ini.Read("DBPass", "Section1"));
            tbDBName.Text = ini.Read("DBName", "Section1");


            chkGhiNhoTK.Checked = ini.Read("ghi_nho_nguoi_dung", "Section1") == "1" ? true : false;
            chkGhiNhoTKVaMK.Checked = ini.Read("nghi_nho_nd_va_mk", "Section1") == "1" ? true : false;

            if (chkGhiNhoTK.Checked)
                txtTenTK.Text = ini.Read("user_name", "Section1");

            if (chkGhiNhoTK.Checked & chkGhiNhoTKVaMK.Checked)
            {
                txtTenTK.Text = ini.Read("user_name", "Section1");
                txtMatKhau.Text = ini.Read("password", "Section1");
            }

        }

        private void btSignUp_Click(object sender, EventArgs e)
        {
            Hide();
            fSignUp f = new fSignUp();
            f.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (tbDBName.Visible)
            {

                button1.Text = "Hiện Server";
                tbServer.Hide();
                tbDBPass.Hide();
                tbDBName.Hide();
                lbServer.Hide();
                lbDBName.Hide();
                lbDBPass.Hide();
                this.Size = new Size(455, 230);
            }
            else
            {
                button1.Text = "Ẩn server";

                tbServer.Show();
                tbDBPass.Show();
                tbDBName.Show();
                lbServer.Show();
                lbDBName.Show();
                lbDBPass.Show();
                this.Size = new Size(455, 310);
            }
        }
    }
}