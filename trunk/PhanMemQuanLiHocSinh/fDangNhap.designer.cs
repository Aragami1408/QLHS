﻿namespace PhanMemQuanLiHocSinh
{
    partial class fDangNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDangNhap = new System.Windows.Forms.Button();
            this.txtMatKhau = new System.Windows.Forms.TextBox();
            this.txtTenTK = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btSignUp = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.chkGhiNhoTK = new System.Windows.Forms.CheckBox();
            this.chkGhiNhoTKVaMK = new System.Windows.Forms.CheckBox();
            this.lbServer = new System.Windows.Forms.Label();
            this.lbDBName = new System.Windows.Forms.Label();
            this.lbDBPass = new System.Windows.Forms.Label();
            this.tbServer = new System.Windows.Forms.TextBox();
            this.tbDBName = new System.Windows.Forms.TextBox();
            this.tbDBPass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnDangNhap
            // 
            this.btnDangNhap.Location = new System.Drawing.Point(251, 2);
            this.btnDangNhap.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDangNhap.Name = "btnDangNhap";
            this.btnDangNhap.Size = new System.Drawing.Size(96, 27);
            this.btnDangNhap.TabIndex = 30;
            this.btnDangNhap.Text = "Đăng nhập";
            this.btnDangNhap.UseVisualStyleBackColor = true;
            this.btnDangNhap.Click += new System.EventHandler(this.btnDangNhap_Click);
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMatKhau.Location = new System.Drawing.Point(152, 78);
            this.txtMatKhau.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.PasswordChar = '*';
            this.txtMatKhau.Size = new System.Drawing.Size(271, 23);
            this.txtMatKhau.TabIndex = 10;
            // 
            // txtTenTK
            // 
            this.txtTenTK.BackColor = System.Drawing.SystemColors.Info;
            this.txtTenTK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTenTK.Location = new System.Drawing.Point(152, 51);
            this.txtTenTK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTenTK.Name = "txtTenTK";
            this.txtTenTK.Size = new System.Drawing.Size(271, 23);
            this.txtTenTK.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 11F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 11F));
            this.tableLayoutPanel1.Controls.Add(this.txtTenTK, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtMatKhau, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.chkGhiNhoTK, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.chkGhiNhoTKVaMK, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.lbServer, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.lbDBName, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.lbDBPass, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.tbServer, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.tbDBName, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.tbDBPass, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 10);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 12;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(437, 366);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(68, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 27);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tài khoản:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(68, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 27);
            this.label2.TabIndex = 4;
            this.label2.Text = "Mật khẩu:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 5);
            this.flowLayoutPanel1.Controls.Add(this.btnThoat);
            this.flowLayoutPanel1.Controls.Add(this.btnDangNhap);
            this.flowLayoutPanel1.Controls.Add(this.btSignUp);
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 333);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(431, 31);
            this.flowLayoutPanel1.TabIndex = 30;
            // 
            // btnThoat
            // 
            this.btnThoat.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnThoat.Location = new System.Drawing.Point(353, 2);
            this.btnThoat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 27);
            this.btnThoat.TabIndex = 40;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btSignUp
            // 
            this.btSignUp.Location = new System.Drawing.Point(170, 2);
            this.btSignUp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSignUp.Name = "btSignUp";
            this.btSignUp.Size = new System.Drawing.Size(75, 27);
            this.btSignUp.TabIndex = 41;
            this.btSignUp.Text = "Đăng kí";
            this.btSignUp.UseVisualStyleBackColor = true;
            this.btSignUp.Click += new System.EventHandler(this.btSignUp_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(81, 2);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 27);
            this.button1.TabIndex = 42;
            this.button1.Text = "Ẩn server";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chkGhiNhoTK
            // 
            this.chkGhiNhoTK.AutoSize = true;
            this.chkGhiNhoTK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkGhiNhoTK.Location = new System.Drawing.Point(152, 105);
            this.chkGhiNhoTK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkGhiNhoTK.Name = "chkGhiNhoTK";
            this.chkGhiNhoTK.Size = new System.Drawing.Size(271, 20);
            this.chkGhiNhoTK.TabIndex = 20;
            this.chkGhiNhoTK.Text = "Ghi nhớ tài khoản";
            this.chkGhiNhoTK.UseVisualStyleBackColor = true;
            // 
            // chkGhiNhoTKVaMK
            // 
            this.chkGhiNhoTKVaMK.AutoSize = true;
            this.chkGhiNhoTKVaMK.Location = new System.Drawing.Point(152, 129);
            this.chkGhiNhoTKVaMK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkGhiNhoTKVaMK.Name = "chkGhiNhoTKVaMK";
            this.chkGhiNhoTKVaMK.Size = new System.Drawing.Size(229, 20);
            this.chkGhiNhoTKVaMK.TabIndex = 25;
            this.chkGhiNhoTKVaMK.Text = "Ghi nhớ tài khoản và mật khẩu";
            this.chkGhiNhoTKVaMK.UseVisualStyleBackColor = true;
            // 
            // lbServer
            // 
            this.lbServer.AutoSize = true;
            this.lbServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbServer.ForeColor = System.Drawing.Color.Red;
            this.lbServer.Location = new System.Drawing.Point(68, 151);
            this.lbServer.Name = "lbServer";
            this.lbServer.Size = new System.Drawing.Size(78, 27);
            this.lbServer.TabIndex = 31;
            this.lbServer.Text = "Server";
            this.lbServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDBName
            // 
            this.lbDBName.AutoSize = true;
            this.lbDBName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDBName.ForeColor = System.Drawing.Color.Red;
            this.lbDBName.Location = new System.Drawing.Point(68, 178);
            this.lbDBName.Name = "lbDBName";
            this.lbDBName.Size = new System.Drawing.Size(78, 27);
            this.lbDBName.TabIndex = 32;
            this.lbDBName.Text = "DBName";
            this.lbDBName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDBPass
            // 
            this.lbDBPass.AutoSize = true;
            this.lbDBPass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDBPass.Location = new System.Drawing.Point(68, 205);
            this.lbDBPass.Name = "lbDBPass";
            this.lbDBPass.Size = new System.Drawing.Size(78, 27);
            this.lbDBPass.TabIndex = 33;
            this.lbDBPass.Text = "DBPass";
            this.lbDBPass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbServer
            // 
            this.tbServer.BackColor = System.Drawing.SystemColors.Info;
            this.tbServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbServer.Location = new System.Drawing.Point(152, 153);
            this.tbServer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbServer.Name = "tbServer";
            this.tbServer.Size = new System.Drawing.Size(271, 23);
            this.tbServer.TabIndex = 34;
            // 
            // tbDBName
            // 
            this.tbDBName.BackColor = System.Drawing.SystemColors.Info;
            this.tbDBName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDBName.Location = new System.Drawing.Point(152, 180);
            this.tbDBName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDBName.Name = "tbDBName";
            this.tbDBName.Size = new System.Drawing.Size(271, 23);
            this.tbDBName.TabIndex = 35;
            // 
            // tbDBPass
            // 
            this.tbDBPass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDBPass.Location = new System.Drawing.Point(152, 207);
            this.tbDBPass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDBPass.Name = "tbDBPass";
            this.tbDBPass.Size = new System.Drawing.Size(271, 23);
            this.tbDBPass.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel1.SetColumnSpan(this.label3, 5);
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(437, 39);
            this.label3.TabIndex = 38;
            this.label3.Text = "Phần mềm quản lí học sinh";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::PhanMemQuanLiHocSinh.Properties.Resources.LoginUser2_36x36;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(14, 51);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.tableLayoutPanel1.SetRowSpan(this.pictureBox1, 2);
            this.pictureBox1.Size = new System.Drawing.Size(48, 50);
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel1.SetColumnSpan(this.label4, 5);
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 329);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(431, 2);
            this.label4.TabIndex = 39;
            // 
            // fDangNhap
            // 
            this.AcceptButton = this.btnDangNhap;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnThoat;
            this.ClientSize = new System.Drawing.Size(437, 366);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fDangNhap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng nhập";
            this.Load += new System.EventHandler(this.fDangNhap_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDangNhap;
        private System.Windows.Forms.TextBox txtMatKhau;
        private System.Windows.Forms.TextBox txtTenTK;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.CheckBox chkGhiNhoTK;
        private System.Windows.Forms.CheckBox chkGhiNhoTKVaMK;
        private System.Windows.Forms.Button btSignUp;
        private System.Windows.Forms.Label lbServer;
        private System.Windows.Forms.Label lbDBName;
        private System.Windows.Forms.Label lbDBPass;
        private System.Windows.Forms.TextBox tbServer;
        private System.Windows.Forms.TextBox tbDBName;
        private System.Windows.Forms.TextBox tbDBPass;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

