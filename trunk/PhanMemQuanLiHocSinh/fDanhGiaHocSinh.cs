﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDanhGiaHocSinh : Form
    {
        public fDanhGiaHocSinh()
        {
            InitializeComponent();
        }

        private void btThemDanhGia_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"server=.\SQLEXPRESS;database=QLHS;uid=sa;password=sa123");
            con.Open();

            // TODO: Check null
            if (String.IsNullOrEmpty(tbMaHS.Text))
            {
                MessageBox.Show("Mời bạn nhập mã học sinh", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbTenHS.Text))
            {
                MessageBox.Show("Mời bạn nhập tên học sinh", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            //else if (String.IsNullOrEmpty(tbMonHoc.Text))
            //{
            //    MessageBox.Show("Mời bạn nhập môn học cần đánh giá", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
            else if (String.IsNullOrEmpty(tbDiemMieng.Text))
            {
                MessageBox.Show("Mời bạn nhập điểm miệng của môn học đó", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbDiem15Phut.Text))
            {
                MessageBox.Show("Mời bạn nhập các điểm 15 phút của môn học", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbDiem45Phut.Text))
            {
                MessageBox.Show("Mời bạn nhập các điểm 45 phút của môn học", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbTuan1.Text))
            {
                MessageBox.Show("Mời bạn nhập điểm thi đua tuần 1", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbTuan2.Text))
            {
                MessageBox.Show("Mời bạn nhập điểm thi đua tuần 2", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbTuan3.Text))
            {
                MessageBox.Show("Mời bạn nhập điểm thi đua tuần 3", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbTuan4.Text))
            {
                MessageBox.Show("Mời bạn nhập điểm thi đua tuần 4", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbNhanXetCBL.Text))
            {
                MessageBox.Show("Mời bạn nhập nhận xét của cán bộ lớp", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbNhanXetGVCN.Text))
            {
                MessageBox.Show("Mời bạn nhập nhận xét của giáo viên chủ nhiệm", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbNhanXetCMHS.Text))
            {
                MessageBox.Show("Mời bạn nhập nhận xét của cha mẹ học sinh", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                try
                {
                    string insert = "insert into tDG(cMaHS, cTenHS, dThang, dNam, cMonHoc, cDiemMieng, cDiem15, cDiem45, nTuan1, nTuan2, nTuan3, nTuan4, cNhanXetCB, cNhanXetGVCN, cNhanXetCMHS) values(@cMaHS, @cTenHS, @dThang, @dNam, @cMonHoc, @cDiemMieng, @cDiem15, @cDiem45, @nTuan1, @nTuan2, @nTuan3, @nTuan4, @cNhanXetCB, @cNhanXetGVCN, @cNhanXetCMHS)";
                    SqlCommand cmd = new SqlCommand(insert, con);
                    cmd.Parameters.AddWithValue("@cMaHS", tbMaHS.Text);
                    cmd.Parameters.AddWithValue("@cTenHS", tbTenHS.Text);
                    cmd.Parameters.AddWithValue("@dThang", dateTimePicker1.Value.Month);
                    cmd.Parameters.AddWithValue("@dNam", dateTimePicker1.Value.Year);
                    //cmd.Parameters.AddWithValue("@cMonHoc", tbMonHoc.Text);
                    cmd.Parameters.AddWithValue("@cDiemMieng", tbDiemMieng.Text);
                    cmd.Parameters.AddWithValue("@cDiem15", tbDiem15Phut.Text);
                    cmd.Parameters.AddWithValue("@cDiem45", tbDiem45Phut.Text);
                    cmd.Parameters.AddWithValue("@nTuan1", tbTuan1.Text);
                    cmd.Parameters.AddWithValue("@nTuan2", tbTuan2.Text);
                    cmd.Parameters.AddWithValue("@nTuan3", tbTuan3.Text);
                    cmd.Parameters.AddWithValue("@nTuan4", tbTuan4.Text);
                    cmd.Parameters.AddWithValue("@cNhanXetCB", tbNhanXetCBL.Text);
                    cmd.Parameters.AddWithValue("@cNhanXetGVCN", tbNhanXetGVCN.Text);
                    cmd.Parameters.AddWithValue("@cNhanXetCMHS", tbNhanXetCMHS.Text);
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException SE)
                {
                    MessageBox.Show(SE.Message, "ERROR FOUND!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }


            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            Close();
            Dispose();
        }
    }
}