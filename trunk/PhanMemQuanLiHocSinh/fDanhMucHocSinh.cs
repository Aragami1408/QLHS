﻿using Phần_mềm_quản_lý_học_sinh;
using Phần_mềm_quản_lý_học_sinh.Form_folder.MainForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDanhMucHocSinh : Form
    {
        /// <summary>
        /// Hiển thị danh mục học sinh
        /// </summary>
        public fDanhMucHocSinh()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Xử lí sự kiện click của button Cancel
        /// </summary>
        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
            Dispose();
        }
        /// <summary>
        /// Xử lí sự kiện của button Click
        /// </summary>
        private void btAddStudent_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"server=.\SQLEXPRESS;database=QLHS;uid=sa;password=sa123");
            try
            {
                conn.Open();

                if (String.IsNullOrWhiteSpace(tbMaHocSinh.Text))
                {
                    MessageBox.Show("Mời bạn nhập mã", "BẮT BUỘC!!! ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (String.IsNullOrWhiteSpace(tbStudentName.Text))
                {
                    MessageBox.Show("Mời bạn nhập tên", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (String.IsNullOrWhiteSpace(tbDanToc.Text))
                {
                    MessageBox.Show("Mời bạn nhập dân tộc", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Question);
                }
                else
                {


                    string insert = @"INSERT INTO tDMHocSinh(cMaHS, cTenHocSinh, dNgaySinh, cGioiTinh, cDanToc) VALUES (@cMaHS, @cTenHocSinh, @dNgaySinh, @cGioiTinh, @cDanToc)";
                    SqlCommand cmd = new SqlCommand(insert, conn);

                    cmd.Parameters.AddWithValue("@cMaHS", tbMaHocSinh.Text);
                    cmd.Parameters.AddWithValue("@cTenHocSinh", tbStudentName.Text);
                    cmd.Parameters.AddWithValue("@dNgaySinh", dtpNgaySinh.Value);
                    //cmd.Parameters.AddWithValue("@cGioiTinh", tbGioiTinh.Text);
                    cmd.Parameters.AddWithValue("@cDanToc", tbDanToc.Text);
                    cmd.ExecuteNonQuery();



                    MessageBox.Show("Bạn đã nhập thành công", "CHÚC MỪNG!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }

            catch (SqlException sqlexp)
            {
                MessageBox.Show(sqlexp.Message, "Error found!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                throw;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
                

            }

        

        private void lbDanhMucMonHoc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            fDanhMucMonHoc fMonHoc = new fDanhMucMonHoc();
            fMonHoc.Show();
        }

        private void btnReturnToMenu_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lbSoYeuLiLich_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Hide();
            fSYLL f = new fSYLL();
            f.Show();
        }

        private void lbSCHOOLInformation_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Hide();
            fTTTL f = new fTTTL();
            f.Show();
        }

        private void lbMain_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Hide();
            F_Main f = new F_Main();
            f.Show();
        }
    }
}


