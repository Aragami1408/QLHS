﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhanMemQuanLiHocSinh
{
    public partial class fDanhMucMonHoc : Form
    {
        public string MaMonHoc { get; set; }

        public fDanhMucMonHoc()
        {
            InitializeComponent();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btBackToMenu_Click(object sender, EventArgs e)
        {
            Hide();
            fDangNhap f = new fDangNhap();
            f.Show();
        }

        private void btThemMonHoc_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi);
            conn.Open();
            if (String.IsNullOrEmpty(tbMaMonHoc.Text))
            {
                MessageBox.Show("Mời bạn nhập mã môn học", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbMonHoc.Text))
            {
                MessageBox.Show("Mời bạn nhập môn học", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (String.IsNullOrEmpty(tbTeacher.Text))
            {
                MessageBox.Show("Mời bạn nhập tên giáo viên", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                try
                {
                    string queryInsert = "INSERT INTO tDMMonHoc(cMaMH, cTenMH, cGiaoVienDay) VALUES (@cMaMH, @cTenMH, @cGiaoVienDay)";
                    string query = "SELECT cTenMH FROM tDMMonHoc WHERE cMaMH = '" + tbMaMonHoc.Text.Trim() + "' AND cMaMH <> '" + this.MaMonHoc + "'";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    SqlDataReader rd = cmd.ExecuteReader();
                    if (rd.Read())
                    {
                        MessageBox.Show("Mã này đã có, xin mời bạn nhập lại");
                        tbMaMonHoc.Focus();
                        rd.Close();
                    }
                    else
                    {
                        rd.Close();

                        if (string.IsNullOrEmpty(this.MaMonHoc))
                            cmd.CommandText = queryInsert;
                        else
                            cmd.CommandText = "UPDATE tDMMonHoc set cMaMH = @cMaMH, cTenMH = @cTenMH, cGiaoVienDay = @cGiaoVienDay where cMaMH = '" + MaMonHoc + "'";

                        cmd.Parameters.AddWithValue("@cMaMH", tbMaMonHoc.Text);
                        cmd.Parameters.AddWithValue("@cTenMH", tbMonHoc.Text);
                        cmd.Parameters.AddWithValue("@cGiaoVienDay", tbTeacher.Text);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Bạn đã thêm môn học thành công", "ADDING DATA IS COMPLETE!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                    }

                }
                catch (SqlException sqlexp)
                {
                    MessageBox.Show(sqlexp.Message, "ERROR FOUND!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    throw;
                }
                finally
                {
                    conn.Close();
                    conn.Dispose();
                }

            }

        }

        private void fDanhMucMonHoc_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.MaMonHoc))
            {
                SqlConnection con = new SqlConnection(QLHSSupporter.DatabaseHelper.ChuoiKetNoi);
                con.Open();

                SqlCommand cmd = new SqlCommand("Select * FROM tDMMonHoc WHERE cMaMH =" + this.MaMonHoc);
                cmd.Connection = con;
                SqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    tbMaMonHoc.Text = rd["cMaMH"].ToString();
                    tbMonHoc.Text = rd["cTenMH"].ToString();
                    tbTeacher.Text = rd["cGiaoVienDay"].ToString();

                }
                rd.Close();
                con.Close();
                con.Dispose();
            }

        }
    }
}
