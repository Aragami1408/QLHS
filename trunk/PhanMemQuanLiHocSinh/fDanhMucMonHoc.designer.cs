﻿namespace PhanMemQuanLiHocSinh
{
    partial class fDanhMucMonHoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbMonHoc = new System.Windows.Forms.TextBox();
            this.tbTeacher = new System.Windows.Forms.TextBox();
            this.lbMonHoc = new System.Windows.Forms.Label();
            this.lbTeacher = new System.Windows.Forms.Label();
            this.lbMaMonHoc = new System.Windows.Forms.Label();
            this.tbMaMonHoc = new System.Windows.Forms.TextBox();
            this.lbTille = new System.Windows.Forms.Label();
            this.btCancel = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btThemMonHoc = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbMonHoc
            // 
            this.tbMonHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMonHoc.Location = new System.Drawing.Point(106, 69);
            this.tbMonHoc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbMonHoc.Name = "tbMonHoc";
            this.tbMonHoc.Size = new System.Drawing.Size(471, 23);
            this.tbMonHoc.TabIndex = 0;
            // 
            // tbTeacher
            // 
            this.tbTeacher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTeacher.Location = new System.Drawing.Point(106, 96);
            this.tbTeacher.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbTeacher.Name = "tbTeacher";
            this.tbTeacher.Size = new System.Drawing.Size(471, 23);
            this.tbTeacher.TabIndex = 1;
            // 
            // lbMonHoc
            // 
            this.lbMonHoc.AutoSize = true;
            this.lbMonHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMonHoc.Location = new System.Drawing.Point(3, 67);
            this.lbMonHoc.Name = "lbMonHoc";
            this.lbMonHoc.Size = new System.Drawing.Size(97, 27);
            this.lbMonHoc.TabIndex = 2;
            this.lbMonHoc.Text = "Môn học";
            // 
            // lbTeacher
            // 
            this.lbTeacher.AutoSize = true;
            this.lbTeacher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTeacher.Location = new System.Drawing.Point(3, 94);
            this.lbTeacher.Name = "lbTeacher";
            this.lbTeacher.Size = new System.Drawing.Size(97, 27);
            this.lbTeacher.TabIndex = 3;
            this.lbTeacher.Text = "Giáo viên dạy";
            // 
            // lbMaMonHoc
            // 
            this.lbMaMonHoc.AutoSize = true;
            this.lbMaMonHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMaMonHoc.ForeColor = System.Drawing.Color.Red;
            this.lbMaMonHoc.Location = new System.Drawing.Point(3, 40);
            this.lbMaMonHoc.Name = "lbMaMonHoc";
            this.lbMaMonHoc.Size = new System.Drawing.Size(97, 27);
            this.lbMaMonHoc.TabIndex = 4;
            this.lbMaMonHoc.Text = "Mã môn học";
            this.lbMaMonHoc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbMaMonHoc
            // 
            this.tbMaMonHoc.BackColor = System.Drawing.SystemColors.Info;
            this.tbMaMonHoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMaMonHoc.Location = new System.Drawing.Point(106, 42);
            this.tbMaMonHoc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbMaMonHoc.Name = "tbMaMonHoc";
            this.tbMaMonHoc.Size = new System.Drawing.Size(471, 23);
            this.tbMaMonHoc.TabIndex = 5;
            // 
            // lbTille
            // 
            this.lbTille.AutoSize = true;
            this.lbTille.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.tableLayoutPanel1.SetColumnSpan(this.lbTille, 2);
            this.lbTille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTille.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTille.ForeColor = System.Drawing.Color.White;
            this.lbTille.Location = new System.Drawing.Point(0, 0);
            this.lbTille.Margin = new System.Windows.Forms.Padding(0);
            this.lbTille.Name = "lbTille";
            this.lbTille.Size = new System.Drawing.Size(580, 40);
            this.lbTille.TabIndex = 6;
            this.lbTille.Text = "DANH MỤC MÔN HỌC";
            this.lbTille.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btCancel
            // 
            this.btCancel.Location = new System.Drawing.Point(290, 2);
            this.btCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(105, 28);
            this.btCancel.TabIndex = 9;
            this.btCancel.Text = "Thoát";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbMaMonHoc, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbMonHoc, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbMonHoc, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbTeacher, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbTeacher, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbMaMonHoc, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbTille, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(580, 217);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btCancel, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btThemMonHoc, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 123);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(574, 92);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // btThemMonHoc
            // 
            this.btThemMonHoc.BackgroundImage = global::PhanMemQuanLiHocSinh.Properties.Resources.save;
            this.btThemMonHoc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btThemMonHoc.Location = new System.Drawing.Point(179, 2);
            this.btThemMonHoc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btThemMonHoc.Name = "btThemMonHoc";
            this.btThemMonHoc.Size = new System.Drawing.Size(105, 28);
            this.btThemMonHoc.TabIndex = 7;
            this.btThemMonHoc.Text = "Lưu ";
            this.btThemMonHoc.UseVisualStyleBackColor = true;
            this.btThemMonHoc.Click += new System.EventHandler(this.btThemMonHoc_Click);
            // 
            // fDanhMucMonHoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 217);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "fDanhMucMonHoc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fDanhMucMonHoc";
            this.Load += new System.EventHandler(this.fDanhMucMonHoc_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbMonHoc;
        private System.Windows.Forms.TextBox tbTeacher;
        private System.Windows.Forms.Label lbMonHoc;
        private System.Windows.Forms.Label lbTeacher;
        private System.Windows.Forms.Label lbMaMonHoc;
        private System.Windows.Forms.TextBox tbMaMonHoc;
        private System.Windows.Forms.Label lbTille;
        private System.Windows.Forms.Button btThemMonHoc;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}