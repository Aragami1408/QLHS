﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Phần_mềm_quản_lý_học_sinh.Form_folder.MainForm
{
    public partial class fSYLL : Form
    {
        
        public fSYLL()
        {
            
            InitializeComponent();
        }
        private void btAddSYLL_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"server=.\SQLEXPRESS;database=QLHS;uid=sa;password=sa123");
            con.Open();

            if (String.IsNullOrEmpty(cMaHSTextBox.Text) || String.IsNullOrWhiteSpace(cMaHSTextBox.Text))
            {
                MessageBox.Show("cMaHSTextBox.Text is required!!!", "Are you a member in QLHS???", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cMaHSTextBox.Focus();
            }
            else if (String.IsNullOrEmpty(cTenHSTextBox.Text))
            {
                MessageBox.Show("cTenHSTextBox.Text is required!!!", "you ought to ask your mom 'WHAT IS MY NAME???'", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cTenHSTextBox.Focus();
            }
            //else if (String.IsNullOrEmpty(cGioiTinhTextBox.Text))
            //{
            //    MessageBox.Show("cGioiTinhTextBox.Text is required", "Are you gay, les,... But whatever, write it naturally");
            //    cGioiTinhTextBox.Focus();
            //}
            else if (String.IsNullOrEmpty(cNoiOTextBox.Text))
            {
                MessageBox.Show("cNoiOTextBox.Text is required", "Are you homeless", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cNoiOTextBox.Focus();
            }
            else if (String.IsNullOrEmpty(cUutienTextBox.Text))
            {
                MessageBox.Show("cUuTienTextBox.Text", "Didn't you have a prioritize policy", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cUutienTextBox.Focus();
            }
            else
            {
                try
                {
                    string insert = @"insert into tSYLL(cMaHS, cTenHS, cGioiTinh, dNgaySinh, cNoiSinh, cDanToc, cNoiO, cUuTien) values(@cMaHS, @cTenHS, @cGioiTinh, @dNgaySinh, @cNoiSinh, @cDanToc, @cNoiO, @cUuTien)";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    cmd.Parameters.AddWithValue("@cMaHS", cMaHSTextBox.Text);
                    cmd.Parameters.AddWithValue("@cTenHS", cTenHSTextBox.Text);
                    //cmd.Parameters.AddWithValue("@cGioiTinh", cGioiTinhTextBox.Text);
                    cmd.Parameters.AddWithValue("@dNgaySinh", dNgaySinhDateTimePicker.Value);
                    cmd.Parameters.AddWithValue("@cNoiSinh", cNoiOTextBox.Text);
                    cmd.Parameters.AddWithValue("@cDanToc", cDanTocTextBox.Text);
                    cmd.Parameters.AddWithValue("@cNoiO", cNoiOTextBox.Text);
                    cmd.Parameters.AddWithValue("@cUuTien", cUutienTextBox.Text);
                    cmd.ExecuteNonQuery();
                    
                }
                catch (SqlException SE)
                {
                    MessageBox.Show(SE.Message);
                }
                con.Close();
                con.Dispose();
            }
        }
    }
}