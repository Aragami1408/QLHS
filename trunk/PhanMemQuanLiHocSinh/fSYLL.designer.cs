﻿namespace Phần_mềm_quản_lý_học_sinh.Form_folder.MainForm
{
    partial class fSYLL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cMaHSLabel;
            System.Windows.Forms.Label cTenHSLabel;
            System.Windows.Forms.Label cGioiTinhLabel;
            System.Windows.Forms.Label dNgaySinhLabel;
            System.Windows.Forms.Label cNoiSinhLabel;
            System.Windows.Forms.Label cDanTocLabel;
            System.Windows.Forms.Label cNoiOLabel;
            System.Windows.Forms.Label cUutienLabel;
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cMaHSTextBox = new System.Windows.Forms.TextBox();
            this.cTenHSTextBox = new System.Windows.Forms.TextBox();
            this.dNgaySinhDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.cNoiSinhTextBox = new System.Windows.Forms.TextBox();
            this.cDanTocTextBox = new System.Windows.Forms.TextBox();
            this.cNoiOTextBox = new System.Windows.Forms.TextBox();
            this.cUutienTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btAddSYLL = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tQHGDBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            cMaHSLabel = new System.Windows.Forms.Label();
            cTenHSLabel = new System.Windows.Forms.Label();
            cGioiTinhLabel = new System.Windows.Forms.Label();
            dNgaySinhLabel = new System.Windows.Forms.Label();
            cNoiSinhLabel = new System.Windows.Forms.Label();
            cDanTocLabel = new System.Windows.Forms.Label();
            cNoiOLabel = new System.Windows.Forms.Label();
            cUutienLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tQHGDBindingSource)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cMaHSLabel
            // 
            cMaHSLabel.AutoSize = true;
            cMaHSLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            cMaHSLabel.ForeColor = System.Drawing.Color.Red;
            cMaHSLabel.Location = new System.Drawing.Point(4, 0);
            cMaHSLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            cMaHSLabel.Name = "cMaHSLabel";
            cMaHSLabel.Size = new System.Drawing.Size(99, 31);
            cMaHSLabel.TabIndex = 1;
            cMaHSLabel.Text = "Mã học sinh:";
            cMaHSLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cTenHSLabel
            // 
            cTenHSLabel.AutoSize = true;
            cTenHSLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            cTenHSLabel.ForeColor = System.Drawing.Color.Red;
            cTenHSLabel.Location = new System.Drawing.Point(4, 31);
            cTenHSLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            cTenHSLabel.Name = "cTenHSLabel";
            cTenHSLabel.Size = new System.Drawing.Size(99, 31);
            cTenHSLabel.TabIndex = 3;
            cTenHSLabel.Text = "Tên học sinh:";
            cTenHSLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cGioiTinhLabel
            // 
            cGioiTinhLabel.AutoSize = true;
            cGioiTinhLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            cGioiTinhLabel.Location = new System.Drawing.Point(4, 62);
            cGioiTinhLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            cGioiTinhLabel.Name = "cGioiTinhLabel";
            cGioiTinhLabel.Size = new System.Drawing.Size(99, 31);
            cGioiTinhLabel.TabIndex = 5;
            cGioiTinhLabel.Text = "Giới tính:";
            cGioiTinhLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dNgaySinhLabel
            // 
            dNgaySinhLabel.AutoSize = true;
            dNgaySinhLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            dNgaySinhLabel.Location = new System.Drawing.Point(4, 93);
            dNgaySinhLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            dNgaySinhLabel.Name = "dNgaySinhLabel";
            dNgaySinhLabel.Size = new System.Drawing.Size(99, 31);
            dNgaySinhLabel.TabIndex = 7;
            dNgaySinhLabel.Text = "Ngày sinh";
            dNgaySinhLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cNoiSinhLabel
            // 
            cNoiSinhLabel.AutoSize = true;
            cNoiSinhLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            cNoiSinhLabel.Location = new System.Drawing.Point(4, 124);
            cNoiSinhLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            cNoiSinhLabel.Name = "cNoiSinhLabel";
            cNoiSinhLabel.Size = new System.Drawing.Size(99, 31);
            cNoiSinhLabel.TabIndex = 9;
            cNoiSinhLabel.Text = "Nơi sinh:";
            cNoiSinhLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cDanTocLabel
            // 
            cDanTocLabel.AutoSize = true;
            cDanTocLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            cDanTocLabel.Location = new System.Drawing.Point(4, 155);
            cDanTocLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            cDanTocLabel.Name = "cDanTocLabel";
            cDanTocLabel.Size = new System.Drawing.Size(99, 31);
            cDanTocLabel.TabIndex = 11;
            cDanTocLabel.Text = "Dân tộc";
            cDanTocLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cNoiOLabel
            // 
            cNoiOLabel.AutoSize = true;
            cNoiOLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            cNoiOLabel.Location = new System.Drawing.Point(271, 62);
            cNoiOLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            cNoiOLabel.Name = "cNoiOLabel";
            cNoiOLabel.Size = new System.Drawing.Size(78, 31);
            cNoiOLabel.TabIndex = 13;
            cNoiOLabel.Text = "Nơi Ở";
            cNoiOLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cUutienLabel
            // 
            cUutienLabel.AutoSize = true;
            cUutienLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            cUutienLabel.Location = new System.Drawing.Point(271, 93);
            cUutienLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            cUutienLabel.Name = "cUutienLabel";
            cUutienLabel.Size = new System.Drawing.Size(78, 31);
            cUutienLabel.TabIndex = 15;
            cUutienLabel.Text = "Ưu tiên";
            cUutienLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cMaHSTextBox
            // 
            this.cMaHSTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.tableLayoutPanel1.SetColumnSpan(this.cMaHSTextBox, 3);
            this.cMaHSTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cMaHSTextBox.Location = new System.Drawing.Point(111, 4);
            this.cMaHSTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.cMaHSTextBox.Name = "cMaHSTextBox";
            this.cMaHSTextBox.Size = new System.Drawing.Size(399, 23);
            this.cMaHSTextBox.TabIndex = 2;
            // 
            // cTenHSTextBox
            // 
            this.cTenHSTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.tableLayoutPanel1.SetColumnSpan(this.cTenHSTextBox, 3);
            this.cTenHSTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cTenHSTextBox.Location = new System.Drawing.Point(111, 35);
            this.cTenHSTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.cTenHSTextBox.Name = "cTenHSTextBox";
            this.cTenHSTextBox.Size = new System.Drawing.Size(399, 23);
            this.cTenHSTextBox.TabIndex = 4;
            // 
            // dNgaySinhDateTimePicker
            // 
            this.dNgaySinhDateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.dNgaySinhDateTimePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dNgaySinhDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dNgaySinhDateTimePicker.Location = new System.Drawing.Point(111, 97);
            this.dNgaySinhDateTimePicker.Margin = new System.Windows.Forms.Padding(4);
            this.dNgaySinhDateTimePicker.Name = "dNgaySinhDateTimePicker";
            this.dNgaySinhDateTimePicker.Size = new System.Drawing.Size(152, 23);
            this.dNgaySinhDateTimePicker.TabIndex = 8;
            // 
            // cNoiSinhTextBox
            // 
            this.cNoiSinhTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cNoiSinhTextBox.Location = new System.Drawing.Point(111, 128);
            this.cNoiSinhTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.cNoiSinhTextBox.Name = "cNoiSinhTextBox";
            this.cNoiSinhTextBox.Size = new System.Drawing.Size(152, 23);
            this.cNoiSinhTextBox.TabIndex = 10;
            // 
            // cDanTocTextBox
            // 
            this.cDanTocTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cDanTocTextBox.Location = new System.Drawing.Point(111, 159);
            this.cDanTocTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.cDanTocTextBox.Name = "cDanTocTextBox";
            this.cDanTocTextBox.Size = new System.Drawing.Size(152, 23);
            this.cDanTocTextBox.TabIndex = 12;
            // 
            // cNoiOTextBox
            // 
            this.cNoiOTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cNoiOTextBox.Location = new System.Drawing.Point(357, 66);
            this.cNoiOTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.cNoiOTextBox.Name = "cNoiOTextBox";
            this.cNoiOTextBox.Size = new System.Drawing.Size(153, 23);
            this.cNoiOTextBox.TabIndex = 14;
            // 
            // cUutienTextBox
            // 
            this.cUutienTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cUutienTextBox.Location = new System.Drawing.Point(357, 97);
            this.cUutienTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.cUutienTextBox.Name = "cUutienTextBox";
            this.cUutienTextBox.Size = new System.Drawing.Size(153, 23);
            this.cUutienTextBox.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(534, 50);
            this.label1.TabIndex = 18;
            this.label1.Text = "SƠ YẾU LÍ LỊCH HỌC SINH";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btAddSYLL
            // 
            this.btAddSYLL.Location = new System.Drawing.Point(290, 2);
            this.btAddSYLL.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAddSYLL.Name = "btAddSYLL";
            this.btAddSYLL.Size = new System.Drawing.Size(132, 28);
            this.btAddSYLL.TabIndex = 22;
            this.btAddSYLL.Text = "Thêm học sinh";
            this.btAddSYLL.UseVisualStyleBackColor = true;
            this.btAddSYLL.Click += new System.EventHandler(this.btAddSYLL_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(428, 2);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 28);
            this.button1.TabIndex = 23;
            this.button1.Text = "Thoát";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(223)))), ((int)(((byte)(238)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column5,
            this.Column4});
            this.dataGridView1.DataSource = this.tQHGDBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(129)))), ((int)(((byte)(189)))));
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(514, 189);
            this.dataGridView1.TabIndex = 24;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(cMaHSLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(cTenHSLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(cGioiTinhLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(dNgaySinhLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.cDanTocTextBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(cNoiSinhLabel, 0, 4);
            this.tableLayoutPanel1.Controls.Add(cDanTocLabel, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.cNoiSinhTextBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.cMaHSTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cTenHSTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.dNgaySinhDateTimePicker, 1, 3);
            this.tableLayoutPanel1.Controls.Add(cNoiOLabel, 2, 2);
            this.tableLayoutPanel1.Controls.Add(cUutienLabel, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.cNoiOTextBox, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.cUutienTextBox, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBox1, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox2, 3, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(514, 189);
            this.tableLayoutPanel1.TabIndex = 25;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.Controls.Add(this.radioButton1);
            this.flowLayoutPanel2.Controls.Add(this.radioButton2);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(107, 62);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(160, 31);
            this.flowLayoutPanel2.TabIndex = 52;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(3, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(54, 20);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Nam";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(63, 3);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(44, 20);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Nữ";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(270, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 31);
            this.label2.TabIndex = 53;
            this.label2.Text = "Nhiệm vụ:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(356, 127);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(155, 23);
            this.textBox1.TabIndex = 54;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(270, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 31);
            this.label3.TabIndex = 55;
            this.label3.Text = "Điện thoại:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox2
            // 
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox2.Location = new System.Drawing.Point(356, 158);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(155, 23);
            this.textBox2.TabIndex = 56;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 53);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(528, 224);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(520, 195);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Bản thân";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(520, 195);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Quan hệ gia đình";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tabControl1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(534, 312);
            this.tableLayoutPanel2.TabIndex = 24;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.btAddSYLL);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 280);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(534, 32);
            this.flowLayoutPanel1.TabIndex = 19;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Họ tên";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Năm sinh";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Quan hệ";
            this.Column3.Name = "Column3";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Điện thoại";
            this.Column5.Name = "Column5";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Ghi chú";
            this.Column4.Name = "Column4";
            // 
            // fSYLL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 312);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(550, 350);
            this.Name = "fSYLL";
            this.Text = "Sơ yếu lý lịch";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tQHGDBindingSource)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox cMaHSTextBox;
        private System.Windows.Forms.TextBox cTenHSTextBox;
        private System.Windows.Forms.DateTimePicker dNgaySinhDateTimePicker;
        private System.Windows.Forms.TextBox cNoiSinhTextBox;
        private System.Windows.Forms.TextBox cDanTocTextBox;
        private System.Windows.Forms.TextBox cNoiOTextBox;
        private System.Windows.Forms.TextBox cUutienTextBox;
        private System.Windows.Forms.Label label1;
        //private PhanMemQuanLiHocSinh.QuanLyHocSinhDataSet quanLyHocSinhDataSet;
        //private PhanMemQuanLiHocSinh.QuanLyHocSinhDataSetTableAdapters.tQHGDTableAdapter tQHGDTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn cEmailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDienThoaiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNoiLamViecDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNgheNghiepDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dNamSinhDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cHoTenDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cQHDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cMaHSDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btAddSYLL;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        //private PhanMemQuanLiHocSinh.QuanLyHocSinhDataSet quanLyHocSinhDataSet1;
        private System.Windows.Forms.BindingSource tQHGDBindingSource;
        //private PhanMemQuanLiHocSinh.QuanLyHocSinhDataSetTableAdapters.tQHGDTableAdapter tQHGDTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}