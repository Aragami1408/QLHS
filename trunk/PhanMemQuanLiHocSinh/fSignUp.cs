﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using PhanMemQuanLiHocSinh;

namespace Phần_mềm_quản_lý_học_sinh.Form_folder.MainForm
{
    public partial class fSignUp : Form
    {
        public fSignUp()
        {
            InitializeComponent();
        }

        private void btnDangKy_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"server=.\SQLEXPRESS;database=QLHS;uid=sa;password=sa123");
            con.Open();
            if (String.IsNullOrEmpty(tbAccount.Text))
            {
                MessageBox.Show("Mời bạn nhập lại tài khoản", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (String.IsNullOrEmpty(tbPass.Text))
            {
                MessageBox.Show("Mời bạn nhập lại mật khẩu", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (String.IsNullOrEmpty(tbEmail.Text))
            {
                MessageBox.Show("Mời bạn nhập lại email", "WARNING!!!",MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else if (String.IsNullOrEmpty(tbUSER.Text))
            {
                MessageBox.Show("Mời bạn nhập lại tên người dùng", "WARNING!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    string insert = @"insert into Account([USER NAME], [PASS WORD], ID, EMAIL, ACCOUNT) values(@USER_NAME, @PASS_WORD, @ID, @EMAIL, @ACCOUNT)";
                    SqlCommand cmd = new SqlCommand(insert, con);

                    cmd.Parameters.AddWithValue("@USER_NAME", tbUSER.Text);
                    cmd.Parameters.AddWithValue("@PASS_WORD", tbPass.Text);
                    cmd.Parameters.AddWithValue("@EMAIL", tbEmail.Text);
                    cmd.Parameters.AddWithValue("@ACCOUNT", tbAccount.Text);
                }
                catch (SqlException sqlexp)
                {
                    MessageBox.Show(sqlexp.Message);
                   
                }
                finally
                {
                    con.Close();
                    con.Dispose();

                }
                MessageBox.Show("Bạn đã đăng kí thành công","ADDING ACCOUNT IS COMPLETE!!!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                Hide();
                fDangNhap f = new fDangNhap();
                f.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
