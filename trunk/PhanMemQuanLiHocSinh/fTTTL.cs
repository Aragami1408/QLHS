﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Phần_mềm_quản_lý_học_sinh.Form_folder.MainForm
{
    public partial class fTTTL : Form
    {
        public fTTTL()
        {
            InitializeComponent();
        }

        private void btEscape_Click(object sender, EventArgs e)
        {
            Close();
            Dispose();
        }

        private void btBackToTheMenu_Click(object sender, EventArgs e)
        {
            Hide();
            F_Main f = new F_Main();
            f.Show();
        }

        private void btThemGiaoVien_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"server=.\SQLEXPRESS;database=QLHS;uid=sa;password=sa123");
            conn.Open();

            if (String.IsNullOrEmpty(tbTeacherName.Text))
            {
                MessageBox.Show("Mời bạn nhập lại tên của giáo viên", "Tên giáo viên không được để trống", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (String.IsNullOrEmpty(tbMonDay.Text))
            {
                MessageBox.Show("Mời bạn nhập môn giáo viên dạy", "Nhập đúng tên môn học và không để trống", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (String.IsNullOrEmpty(tbChucVu.Text))
            {
                MessageBox.Show("Mời bạn nhập chức vụ của giáo viên", "Chức vụ không được để trống", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (String.IsNullOrWhiteSpace(tbPhoneNumber.Text) || tbPhoneNumber.Text.Length > 11)
            {
                MessageBox.Show("Mời bạn nhập lại số điện thoại của giáo viên", "Lưu ý:Chỉ nhập số, không được để trống và có dấu cách ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else if (String.IsNullOrWhiteSpace(tbEmail.Text))
            {
                MessageBox.Show("Mời bạn nhập lại email", "Email không nhập đúng định dạng, để trống hoặc có dấu cách ");
            }
            else
            {
                try
                {
                    string query = "INSERT INTO tTTTL(cHoTen, cMonDay, cChucVu, cDienThoai, E-Mail) VALUES (@cHoTEn, @cMonDay, @cChucVu, @cDienThoai, @E-Mail)";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@cHoTen", tbTeacherName.Text);
                    cmd.Parameters.AddWithValue("@cMonDay", tbMonDay.Text);
                    cmd.Parameters.AddWithValue("@cChucVu", tbChucVu.Text);
                    cmd.Parameters.AddWithValue("@cDienThoai", tbPhoneNumber.Text);
                    cmd.Parameters.AddWithValue("@E-Mail", tbEmail.Text);
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException sqlexp)
                {
                    MessageBox.Show(sqlexp.Message);
                }
                finally
                {

                    conn.Close();
                    conn.Dispose();
                }
                MessageBox.Show("Đã thêm thành công ", "Ấn OK để tiếp tục", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
    }
}
