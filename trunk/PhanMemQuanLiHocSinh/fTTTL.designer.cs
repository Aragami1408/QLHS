﻿namespace Phần_mềm_quản_lý_học_sinh.Form_folder.MainForm
{
    partial class fTTTL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTieuDe = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbEmail = new System.Windows.Forms.Label();
            this.lbMonDay = new System.Windows.Forms.Label();
            this.lbDienThoai = new System.Windows.Forms.Label();
            this.lbChucVu = new System.Windows.Forms.Label();
            this.tbTeacherName = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.tbMonDay = new System.Windows.Forms.TextBox();
            this.tbChucVu = new System.Windows.Forms.TextBox();
            this.tbPhoneNumber = new System.Windows.Forms.TextBox();
            this.btThemGiaoVien = new System.Windows.Forms.Button();
            this.btEscape = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbTieuDe
            // 
            this.lbTieuDe.AutoSize = true;
            this.lbTieuDe.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.tableLayoutPanel1.SetColumnSpan(this.lbTieuDe, 2);
            this.lbTieuDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbTieuDe.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTieuDe.ForeColor = System.Drawing.Color.White;
            this.lbTieuDe.Location = new System.Drawing.Point(0, 0);
            this.lbTieuDe.Margin = new System.Windows.Forms.Padding(0);
            this.lbTieuDe.Name = "lbTieuDe";
            this.lbTieuDe.Size = new System.Drawing.Size(534, 50);
            this.lbTieuDe.TabIndex = 0;
            this.lbTieuDe.Text = "THÔNG TIN TRƯỜNG LỚP";
            this.lbTieuDe.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(3, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 27);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên giáo viên";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbEmail
            // 
            this.lbEmail.AutoSize = true;
            this.lbEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbEmail.Location = new System.Drawing.Point(3, 158);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(97, 27);
            this.lbEmail.TabIndex = 3;
            this.lbEmail.Text = "Email";
            this.lbEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbMonDay
            // 
            this.lbMonDay.AutoSize = true;
            this.lbMonDay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMonDay.Location = new System.Drawing.Point(3, 77);
            this.lbMonDay.Name = "lbMonDay";
            this.lbMonDay.Size = new System.Drawing.Size(97, 27);
            this.lbMonDay.TabIndex = 4;
            this.lbMonDay.Text = "Môn dạy";
            this.lbMonDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDienThoai
            // 
            this.lbDienThoai.AutoSize = true;
            this.lbDienThoai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbDienThoai.Location = new System.Drawing.Point(3, 131);
            this.lbDienThoai.Name = "lbDienThoai";
            this.lbDienThoai.Size = new System.Drawing.Size(97, 27);
            this.lbDienThoai.TabIndex = 5;
            this.lbDienThoai.Text = "Điện thoại";
            this.lbDienThoai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbChucVu
            // 
            this.lbChucVu.AutoSize = true;
            this.lbChucVu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbChucVu.Location = new System.Drawing.Point(3, 104);
            this.lbChucVu.Name = "lbChucVu";
            this.lbChucVu.Size = new System.Drawing.Size(97, 27);
            this.lbChucVu.TabIndex = 6;
            this.lbChucVu.Text = "Chức vụ";
            this.lbChucVu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbTeacherName
            // 
            this.tbTeacherName.BackColor = System.Drawing.SystemColors.Info;
            this.tbTeacherName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbTeacherName.Location = new System.Drawing.Point(106, 52);
            this.tbTeacherName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbTeacherName.Name = "tbTeacherName";
            this.tbTeacherName.Size = new System.Drawing.Size(425, 23);
            this.tbTeacherName.TabIndex = 8;
            // 
            // tbEmail
            // 
            this.tbEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbEmail.Location = new System.Drawing.Point(106, 160);
            this.tbEmail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(425, 23);
            this.tbEmail.TabIndex = 9;
            // 
            // tbMonDay
            // 
            this.tbMonDay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMonDay.Location = new System.Drawing.Point(106, 79);
            this.tbMonDay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbMonDay.Name = "tbMonDay";
            this.tbMonDay.Size = new System.Drawing.Size(425, 23);
            this.tbMonDay.TabIndex = 10;
            // 
            // tbChucVu
            // 
            this.tbChucVu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbChucVu.Location = new System.Drawing.Point(106, 106);
            this.tbChucVu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbChucVu.Name = "tbChucVu";
            this.tbChucVu.Size = new System.Drawing.Size(425, 23);
            this.tbChucVu.TabIndex = 11;
            // 
            // tbPhoneNumber
            // 
            this.tbPhoneNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPhoneNumber.Location = new System.Drawing.Point(106, 133);
            this.tbPhoneNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbPhoneNumber.Name = "tbPhoneNumber";
            this.tbPhoneNumber.Size = new System.Drawing.Size(425, 23);
            this.tbPhoneNumber.TabIndex = 12;
            // 
            // btThemGiaoVien
            // 
            this.btThemGiaoVien.Location = new System.Drawing.Point(280, 2);
            this.btThemGiaoVien.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btThemGiaoVien.Name = "btThemGiaoVien";
            this.btThemGiaoVien.Size = new System.Drawing.Size(130, 28);
            this.btThemGiaoVien.TabIndex = 13;
            this.btThemGiaoVien.Text = "Thêm giáo viên";
            this.btThemGiaoVien.UseVisualStyleBackColor = true;
            this.btThemGiaoVien.Click += new System.EventHandler(this.btThemGiaoVien_Click);
            // 
            // btEscape
            // 
            this.btEscape.Location = new System.Drawing.Point(416, 2);
            this.btEscape.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEscape.Name = "btEscape";
            this.btEscape.Size = new System.Drawing.Size(109, 28);
            this.btEscape.TabIndex = 15;
            this.btEscape.Text = "Đóng";
            this.btEscape.UseVisualStyleBackColor = true;
            this.btEscape.Click += new System.EventHandler(this.btEscape_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lbTieuDe, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbMonDay, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbEmail, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbPhoneNumber, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lbChucVu, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbChucVu, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbDienThoai, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tbMonDay, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbEmail, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.tbTeacherName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 7);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(534, 312);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.btEscape);
            this.flowLayoutPanel1.Controls.Add(this.btThemGiaoVien);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 277);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(528, 32);
            this.flowLayoutPanel1.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 272);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(528, 2);
            this.label1.TabIndex = 14;
            // 
            // fTTTL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 312);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(550, 350);
            this.Name = "fTTTL";
            this.Text = "fTTTL";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbTieuDe;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.Label lbMonDay;
        private System.Windows.Forms.Label lbDienThoai;
        private System.Windows.Forms.Label lbChucVu;
        private System.Windows.Forms.TextBox tbTeacherName;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.TextBox tbMonDay;
        private System.Windows.Forms.TextBox tbChucVu;
        private System.Windows.Forms.TextBox tbPhoneNumber;
        private System.Windows.Forms.Button btThemGiaoVien;
        private System.Windows.Forms.Button btEscape;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
    }
}