﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHSSupporter
{
    public class DatabaseHelper : IDisposable
    {
        public static string ChuoiKetNoi;
        public string _chuoiKetNoi;

        List<SqlParameter> Parameters { get; set; }

        public DatabaseHelper()
        {
            _chuoiKetNoi = DatabaseHelper.ChuoiKetNoi;

            this.Parameters = new List<SqlParameter>();
        }

        public DatabaseHelper(string connectionString)
        {
            _chuoiKetNoi = connectionString;
        }

        public SqlDataReader ExecuteReader(string query, params object[] parameters)
        {
            return this.ExecuteReader(query, System.Data.CommandBehavior.CloseConnection, parameters);
        }

        public SqlDataReader ExecuteReader(string query, System.Data.CommandBehavior behavior, params object[] parameters)
        {
            try
            {
                if (parameters.Length % 2 != 0)
                    throw new ArgumentException("Wrong number of parameters sent to procedure. Expected an even number.");

                for (int i = 0; i < parameters.Length; i += 2)
                    this.Parameters.Add(new SqlParameter(parameters[i] as string, parameters[i + 1]));

                SqlConnection conn = new SqlConnection(_chuoiKetNoi);
                SqlCommand command = new SqlCommand(query, conn);

                try
                {
                    if (this.Parameters.Count > 0)
                        command.Parameters.AddRange(this.Parameters.ToArray());

                    //open connection
                    conn.Open();

                    //execute reader
                    return command.ExecuteReader(behavior);
                }
                finally
                {
                    //close connection
                    if (conn != null && conn.State != System.Data.ConnectionState.Open)
                        conn.Close();

                    //clean parameter
                    this.Parameters.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ExecuteNonQuery(string query, params object[] parameters)
        {
            try
            {
                if (parameters.Length % 2 != 0)
                    throw new ArgumentException("Wrong number of parameters sent to procedure. Expected an even number.");

                for (int i = 0; i < parameters.Length; i += 2)
                    this.Parameters.Add(new SqlParameter(parameters[i] as string, parameters[i + 1]));

                SqlConnection conn = new SqlConnection(_chuoiKetNoi);
                SqlCommand command = new SqlCommand(query, conn);

                try
                {
                    if (this.Parameters.Count > 0)
                        command.Parameters.AddRange(this.Parameters.ToArray());

                    //open connection
                    conn.Open();

                    //execute reader
                    return command.ExecuteNonQuery();
                }
                finally
                {
                    //close connection
                    if (conn != null && conn.State != System.Data.ConnectionState.Open)
                        conn.Close();

                    //clean parameter
                    this.Parameters.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SqlParameter AddParameterWithValue(string parameterName, object value)
        {
            SqlParameter p = new SqlParameter(parameterName, value);
            this.Parameters.Add(p);

            return p;
        }

        public void Dispose()
        {
            //todo
        }
    }
}
